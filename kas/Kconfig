# SPDX-FileCopyrightText: Copyright (c) 2024-2025, Linaro Limited.
#
# SPDX-FileCopyrightText: <text>Copyright 2023-2024 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

mainmenu "Cassini Reference Stack Build Config"

#############################
# User-facing configuration #
#############################
comment "Target Build Setup"

menu "Platform Selection"
choice
    prompt "Build Platform"
    default CORSTONE1000_FVP
    help
     Select the build platform you would like your image to run on

config CORSTONE1000_FVP
    bool "Corstone-1000 FVP"
    help
     Build targeting Arm Corstone-1000 FVP

config CORSTONE1000_MPS3
    bool "Corstone-1000 MPS3"
    help
     Build targeting Arm Corstone-1000 for MPS3

config KV260_UBOOT
    bool "Xilinx KV260 with U-Boot"
    help
     Build targeting the Xilinx KV260 Development Platform

config KV260_EDK2
     bool "Xilinx KV260 with EDK2"
     help
      Build targeting the Xilinx KV260 Development Platform

endchoice

comment "End-User License Agreement"
    depends on CORSTONE1000_FVP

config ARM_FVP_EULA_ACCEPT_CHOICE
    depends on CORSTONE1000_FVP
    bool "Accept the END USER LICENSE AGREEMENT FOR ARM SOFTWARE DEVELOPMENT TOOLS"
    default n
    help
      Please refer to "https://developer.arm.com/downloads/-/arm-ecosystem-fvps/eula"
endmenu


menu "Extra Image Features"

menu "Cloud Provider"
choice
    prompt "Cloud Provider Service"
    default CLOUD_PROVIDER_K3s
    help
     Select the cloud service you would like to use with this image

config CLOUD_PROVIDER_NONE
    bool "No cloud Service installed"
    help
     Build not setup for cloud development

config CLOUD_PROVIDER_K3s
    depends on ! CORSTONE1000_FVP && ! CORSTONE1000_MPS3
    bool "Include K3s container orchestration"
    help
     To include K3s container orchestration into the image.

config CLOUD_PROVIDER_GREENGRASS
     bool "Include AWS IoT Greengrass cloud provider"
     help
      To include AWS IoT Greengrass cloud provider into the image.

endchoice
endmenu

menu "Over-the-air update provider"
choice
    prompt "OTA update provider"
    default OTA_PROVIDER_MENDER
    help
     Select the OTA update provider you would like to use with this image

config OTA_PROVIDER_NONE
    bool "No OTA update provider installed"
    help
     Build not setup for OTA update

config OTA_PROVIDER_MENDER
    bool "Include Mender OTA provider"
    help
     To include Mender OTA update provider into the image.

endchoice
endmenu

menu "Security Provider"
choice
    prompt "Security Provider Service"
    default SECURITY_PROVIDER_PSA
    help
     Select the security provider you would like to use with this image

config SECURITY_PROVIDER_PSA
    bool "Use Trusted-Services as Parsec Backend"
    help
     To use Trusted-Services as Parsec Backend

config SECURITY_PROVIDER_SW
    bool "Use Software (MbedCrypto) as Parsec Backend"
    help
     To use software (MbedCrypto) as Parsec Backend.

endchoice
endmenu


config EXTRA_IMAGE_FEATURES_DEV
    bool "Developer Configuration"
    default n
    help
     Configure for developer usage (enable root login, disable Secure Boot)

config EXTRA_IMAGE_FEATURES_TESTS
    bool "Include Integration Tests"
    default n
    help
     To include run-time validation tests into the image.

config RUN_TESTS
    depends on CORSTONE1000_FVP && EXTRA_IMAGE_FEATURES_TESTS
    bool "Run the tests automatically"
    default n
    help
     Run the integration tests on the selected target after
     building the image.

endmenu

config LOCAL_CONFIG
    bool "Include local.yml for local configuration"
    default n
    help
      The expected location is "${KAS_WORK_DIR}/local.yml"

##############
# Set up Kas #
##############

config KAS_INCLUDE_IMAGE
    string
    default "kas/cassini.yml"

config KAS_INCLUDE_PLATFORM
    string
    default "kas/corstone1000-fvp.yml" if CORSTONE1000_FVP
    default "kas/corstone1000-mps3.yml" if CORSTONE1000_MPS3
    default "kas/kv260-psa.yml" if KV260_UBOOT
    default "kas/kv260-edk2.yml" if KV260_EDK2

config KAS_INCLUDE_CLOUD_PROVIDER
    string
    default "" if CLOUD_PROVIDER_K3s
    default "kas/no-cloud.yml" if CLOUD_PROVIDER_NONE
    default "kas/greengrass.yml" if CLOUD_PROVIDER_GREENGRASS

if CLOUD_PROVIDER_NONE
    config VIRTUAL-RUNTIME_cloud_service
        string
        default "no-cloud"
endif

config KAS_INCLUDE_OTA_PROVIDER
    string
    default "kas/no-ota.yml" if OTA_PROVIDER_NONE
    default "kas/mender.yml" if OTA_PROVIDER_MENDER

if OTA_PROVIDER_NONE
    config VIRTUAL-RUNTIME_ota_update
        string
        default "no-ota"
endif

config KAS_INCLUDE_SECURITY_PROVIDER
    string
    default "kas/psa-security.yml" if SECURITY_PROVIDER_PSA
    default "kas/sw-security.yml" if SECURITY_PROVIDER_SW

config KAS_INCLUDE_DEV
    string
    default "kas/dev.yml" if EXTRA_IMAGE_FEATURES_DEV

config KAS_INCLUDE_TESTS
    string
    default "kas/tests.yml" if EXTRA_IMAGE_FEATURES_TESTS

config TESTIMAGE_AUTO
    string
    default "1" if RUN_TESTS
    default "0"

config FVP_CORSTONE1000_EULA_ACCEPT
    string
    default "1" if ARM_FVP_EULA_ACCEPT_CHOICE
    default "0"

config KAS_INCLUDE_LOCAL_CONFIG
    string
    default "local.yml" if LOCAL_CONFIG
