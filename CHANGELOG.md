## 2.1.0 (2024-12-17)

### other (20 changes)

- [cassini-[doc,config,distro]: Cassini 2.1.0 release](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/ee4aae5910f430227b3ab74cafc787fb9b8fdb30)
- [[config,ci,docs]: Refactor KV260 configuration file](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/5711352b14eac0e70651409738c50f85c2d0c636) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/390))
- [ci: Update build/test matrix after review](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/359a211ad717ba2870294d749a8f511956e70840) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/356))
- [[ci,docs]: Align docs build environment with Ubuntu defaults](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/5f38e49237a2b059307160e594a45b7c66135ea8) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/374))
- [ci: Fix rules for test jobs](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/b3702f8617329a812229ced47d77f4e576e6f758) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/350))
- [ci: Add Artifactory rule for Greengrass tests](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/bad56f93b5f9639a40df2b36e667d1098e241adf) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/350))
- [cassini-[distro,tests]: Update LAYERSERIES_COMPAT for `styhead`](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/47237cb2e257e23ef755a15578d18f3790207dda) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/346))
- [[distro,tests]: Add parsec-openssl-provider-tests to ptest](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/0287ecc6d15598185cc3d5f25197a0c94e33234f) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/337))
- [[config,docs]: Remove `k3s.yml` (NFC)](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/0bd8703b89c1fda07803d6889e4b1bcacc131256) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/322))
- [ci: Add ACS tests for KV260](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/a680527652c9d5ad11455ffbcaaa9adea7e38327) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/322))
- [ci: Add manual ACS image build job for KV260](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/73402209eef88bb9c6a26a31cd8535db1c3e6e05) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/322))

### feature (8 changes)

- [config: Add EDK2 build configuration for KV260](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/6f6f09e7c09a3f109935c16ee5d12436bed3add9) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/390))
- [distro: Extend rootfs during boot time](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/e290d8391e5f2adc3482730dbe8243f10aea9f70) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/382))
- [[config,distro]: Modify genericarm64 image build for boot](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/44df64bb16571f268825b0bb845d230810964256) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/285))
- [cassini-distro: Use podman instead of docker-moby](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/d5e2ad3a7cd776c16dd31b5a4c0ae8f684bf7b6a) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/322))

### bug (4 changes)

- [config: Remove cpuidle fix from grub config for KV260](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/2d1721935394729bfcb233e1b02395833b4d6b34) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/388))
- [config: Fix RCU stalling issue for KV260](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/24ac6af533cb08ef5256827836b80737b9a644f5) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/374))

## 2.0.0 (2024-07-24)

### other (41 changes)

- [cassini-[doc,config,distro]: Cassini 2.0.0 release](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/9803dd7961b77b58f420bb1654371949d5f9b35f)
- [docs: Update Yocto layers](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/69add5af128ff23bbd13826f09bbbab938cb3f4e) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/309))
- [docs: Deprecation of Sphinx context injection](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/1dcc5ae958b31b2857008d0832615875192775b2) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/300))
- [docs: Move documentation build instructions to the developer guide](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/052486eddb179c69815eef95a92818fc3cf6aed3) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/295))
- [[ci,config]: Track upstream `scarthgap` release for KV-260](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/feaf2d98b1b57f0071611e63f0a5e91a17f8aaa3) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/274))
- [ci: Add build job for Greengrass Parsec plugin](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/fef07107c5cd5804e6053b1550fa4d8ce9fd6326) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/272))
- [ci: Add Amazon Corretto dev image](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/cb48ebd2ce0c768926ced8028489695b97fbcc0d) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/272))
- [docs: Update kernel version for scarthgap](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/bcd3b0840c3d31585f902966527a0571271cef1a) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/265))
- [docs: Update references to the stable branch](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/a061438c6848391f9c06e56ad0bbeb9e6a704353) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/265))
- [ci: Configure release branch for scarthgap](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/fceeb1229261846c5c322df7e59dd8f1090d249d) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/265))
- [config: Track upstream `scarthgap` release](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/73061cb4c5caf6d060becbabe9a787f6c2aa69f0) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/265))
- [[ci,docs]: Automatically set cassini branch in the documentation](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/cbdfab3b925a20ceed0d0be28909aad425e32345) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/265))
- [cassini-[ci,tests]: Retry pull to avoid TLS timeout in container test](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/9e7a475ce88984593e8d9cd5f3ec7e53e1ca700b) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/265))
- [ci: Enable PARSEC E2E tests on KV260](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/13e0b33137d6e621278569a1690a5fc293009a8c) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/265))
- [ci: Add ptest to KV260 CI](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/5e0f93025a0ca82677191fd807dba44b0785c578) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/256))
- [ci: Don't expose LAVA logs as artifacts](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/0d1d6407e3315c3695c09ae23803f9ae78f1fa9d) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/248))
- [doc: Add KV260 documentation](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/a2483b0b76a503f3f69dbaf5772b4fa6f97f53a3) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/234))
- [cassini-[distro,tests]: Update LAYERSERIES_COMPAT for `scarthgap`](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/37f87f1bde10bb7c45a47ec63018ecac20938701) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/241))
- [ci: Export LAVA console for all LAVA jobs](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/22c7453a5da8222ad611a11b9bb20ec41e77d067) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/231))
- [doc: Docs updated to reference new kas file location](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/0c1b253a0353f19eb991e5a549247b0669ae26c7) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/223))
- [[ci,config] Move kas files](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/26ac03814e9fa59e922c5b89613264026068f7a4) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/223))
- [ci: Project dictionary moved](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/4af9f6dfac02ff47f86d8c06ba204c2c77d940bb) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/223))
- [docs: Update sphinx and dependencies for RTD](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/1065f7cac84a3757d591906222baf393fa4c43e5) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/213))
- [ci: Process test results in script section](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/1f9ef34e871410d60cb8ea3b3d364040c7a1dc5b) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/209))
- [ci: Use variables to configure artifactory layout](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/73d9e8229a4b5192527b17cdd0c8dae3800f0289) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/209))
- [ci: Store ACS results elsewhere and validate correct job retrieved](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/675f9dc60af8286fad13dde833f103b71468c9ad) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/209))
- [ci: Update to the ACS 2.1 test image](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/00c4f7bfcf0bc41ebe75279027ab3290cc34426e) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/209))
- [ci: Update to system ready IR 2.0 result parser](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/da38c2d407ebc508973ab6067a8229394ffa8237) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/209))
- [ci: Fix duplicate entries in the changelog](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/9d2ec5ba70090ba3fa3facda9706e0298e1112eb) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/208))
- [ci: Add jq to the utility image](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/ac1e8bc63336031d51ff3349fda0bd869b1a2933) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/208))
- [cassini-bsp: Use unpadded image for Corstone-1000 FVP](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/c67f29af564c3ff3ed8505b334a5fe094a705dd5) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/207))
- [ci: Use new TUXSUITE_PLATFORMS variable](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/47ffe2c944adfe3b1162b6ea596714c5b89a3be8) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/169))
- [ci: Update to template version 1.6.0](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/841025f7f6611cc8d5669b18572ce18b32123fb7) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/169))
- [config: Update layer name](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/6a95ff810ad67e9e598e351a652e75e389f39e77) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/173))

### bug (7 changes)

- [ci: Update to latest template v1.6.4](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/659eb4723ee8e3929ae9640b9dd1f8484f6cf397)
- [distro: Modify rather than set IMAGE_FEATURES](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/d154885bc68857f24127561ca5a7ba75428fc667) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/297))
- [kas: Ensure url and path are set in one place](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/642e0934a97c8515c2ff92a73182292b08b61627) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/231))
- [[bsp,ci] Update Corstone-1000 build](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/37515afc5d84635a4856640f6905abe139c75c5d) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/227))
- [ci: Use CI_REGISTRY_IMAGE to access docker images](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/cd859d31d1307225db4a15643ea35591a1d6fe02) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/169))

### feature (5 changes)

- [config: Include security hardening by default](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/0a5f4f4e4eef885a19ee0d39ae24c0cf7a8cb9f5) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/292))
- [config: Introduce dev distro feature](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/801dd757f45bc471667ac41a2df3bd98376e6167) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/288))
- [config: Introduce VIRTUAL-RUNTIME_security_provider](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/516802f56afd88237bb8a9684871276b23d40afc) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/282))
- [config: Switch to VIRTUAL-RUNTIME for cloud_service](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/ddffd32b19358658076e11e1b2b9e6e6a22f028c) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/275))
- [cassini-bsp: Use Block Storage SP for Trusted Services on N1SDP](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/f169fda588e6d8a46dee25a1fe85b14d8c45d947) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/185))

### security (1 change)

- [docs: Update Read the Docs search plugin to 0.3.2](https://gitlab.com/Linaro/cassini/meta-cassini/-/commit/25c0a9abfb5484bdb38f74b3f9afb07fd2dc5d68) ([merge request](https://gitlab.com/Linaro/cassini/meta-cassini/-/merge_requests/209))

## 1.1.0 (2023-12-17)

### other (30 changes)

- [cassini-config: Cassini release v1.1.0](Linaro/cassini/meta-cassini@707dfc3b54563cf2d24e09c6f1541d46d1165da3)
- [cassini-distro: Set distro version for release](Linaro/cassini/meta-cassini@9fb145ff022c08edd3aa9e2826203f1ea5df7495)
- [docs: Update Yocto layer documentation](Linaro/cassini/meta-cassini@4742d0a0a32ff7464866098b185f42cb8e364c7e)
- [cassini-config: Pin layers for Cassini v1.1.0](Linaro/cassini/meta-cassini@080046d694a6addf9b282014a737acd9a034b0ce)
- [cassini-[ci,docs]: Use newer PMIC firmware image for N1SDP](Linaro/cassini/meta-cassini@2a5f7d38056153618ea97134b713921798e838c3) ([merge request](Linaro/cassini/meta-cassini!156))
- [docs: Correct note about N1SDP hardware damage](Linaro/cassini/meta-cassini@2fdfb83d2a60f41f569336ad364c3f3be49c0b1e) ([merge request](Linaro/cassini/meta-cassini!156))
- [docs: Update kernel version for nanbield](Linaro/cassini/meta-cassini@77577bcfd0558f324153fa2124694fb24ea55378) ([merge request](Linaro/cassini/meta-cassini!152))
- [docs: Update references to kas to 4.0](Linaro/cassini/meta-cassini@123545cf605ec0c5ba0f461317af16d63c68718a) ([merge request](Linaro/cassini/meta-cassini!152))
- [docs: Update references to Yocto release for nanbield](Linaro/cassini/meta-cassini@75b66743cbabafe07453200ad5a011c92a4024ab) ([merge request](Linaro/cassini/meta-cassini!152))
- [ci: Configure release branch for nanbield](Linaro/cassini/meta-cassini@a158d212b4d347d3a6521b9a6dd4c79d507555af) ([merge request](Linaro/cassini/meta-cassini!152))
- [cassini-config: Track upstream nanbield](Linaro/cassini/meta-cassini@2e2767bf4d407ae4a5518f941ed689c14207a268) ([merge request](Linaro/cassini/meta-cassini!149))
- [docs: Update references to stable branch](Linaro/cassini/meta-cassini@6992bf9fa6b78a00461fabb70e133c14c07397d1) ([merge request](Linaro/cassini/meta-cassini!150))
- [ci: Ensure scheduled test jobs run at a low priority](Linaro/cassini/meta-cassini@c8fdb7f5c76f12e26a583c7a920540a80fc81fad) ([merge request](Linaro/cassini/meta-cassini!145))
- [cassini-[bsp,distro,tests]: Update LAYERSERIES_COMPAT for `nanbield`](Linaro/cassini/meta-cassini@d9d66b6a236b24d4bc1930ab70d95fad46212c51) ([merge request](Linaro/cassini/meta-cassini!117))
- [ci: Increase memory for cassini builds](Linaro/cassini/meta-cassini@13573f3a927efca8ff1a34669257bdf845a3b129) ([merge request](Linaro/cassini/meta-cassini!136))
- [ci: Update to Siemens kas 3.3 in CI](Linaro/cassini/meta-cassini@e19fc445e4e6be98cbc5f49b7670cb3271ed6b12) ([merge request](Linaro/cassini/meta-cassini!116))
- [cassini-distro: Use PACKAGECONFIG to select the Parsec config file](Linaro/cassini/meta-cassini@d05122e9f3ba38576cc0f677a36c1d1e785220a4) ([merge request](Linaro/cassini/meta-cassini!115))
- [cassini-distro: Use PARSEC_CONFIG to configure Parsec](Linaro/cassini/meta-cassini@78305ed73a33aa2384302b7dc196cd9df880e4d5) ([merge request](Linaro/cassini/meta-cassini!115))
- [meta-cassini-distro: Set a DISTROOVERRIDE for cassini features](Linaro/cassini/meta-cassini@7d5f497960afe7cf84eae37e267c3769dc671ba1) ([merge request](Linaro/cassini/meta-cassini!115))
- [[bsp, distro] Stop using c library to determine build type](Linaro/cassini/meta-cassini@93aab01c42fb92f198b2993a0837ccbfb6a58eff) ([merge request](Linaro/cassini/meta-cassini!115))
- [ci: Workaround for QPS error](Linaro/cassini/meta-cassini@6ea32b3fdb986f90faf528375690812ad03b2875) ([merge request](Linaro/cassini/meta-cassini!108))
- [ci: Enable oelint-adv checker](Linaro/cassini/meta-cassini@4b967085ff33dc32bc7b197a9ae26057a3b09813) ([merge request](Linaro/cassini/meta-cassini!108))

### bug (18 changes)

- [[bsp, distro] Move parsec configuration recipe](Linaro/cassini/meta-cassini@3b7fe9dac1c52115dad7593ab1460109629c5072) ([merge request](Linaro/cassini/meta-cassini!151))
- [ci: Ensure layer-checks are run on scheduled builds](Linaro/cassini/meta-cassini@21a8d0f5164a6287d2aca9e438566db8d8ef5c0c) ([merge request](Linaro/cassini/meta-cassini!142))
- [ci: New location for the Corstone-1000 FVP](Linaro/cassini/meta-cassini@972c179a08d4745691d58b3056e19c4003606277) ([merge request](Linaro/cassini/meta-cassini!142))
- [ci: Update GitLab template version](Linaro/cassini/meta-cassini@4cc639de7bd45fa9dba08d602769e8cb232ed98b) ([merge request](Linaro/cassini/meta-cassini!121))
- [cassini-bsp: Increase wait-online timeout for Corstone-1000](Linaro/cassini/meta-cassini@5393829bf8534f8269de719314f409fd17d72787) ([merge request](Linaro/cassini/meta-cassini!115))
- [[bsp, distro] Remove platform specific item from distro layer](Linaro/cassini/meta-cassini@bc6e1ca84518330105df071f2f772af12ccb6370) ([merge request](Linaro/cassini/meta-cassini!115))
- [ci: Update to new template version](Linaro/cassini/meta-cassini@b52b4955bd739d16d31af7062c8314e87dd33f14) ([merge request](Linaro/cassini/meta-cassini!108))
- [ci: Remove scheduled-or-manual rule from all images](Linaro/cassini/meta-cassini@2faf7769dc1f03ac8ffecbd853edcb9a42681e1a) ([merge request](Linaro/cassini/meta-cassini!101))
- [ci: Correct Corstone-1000 MPS3 image build](Linaro/cassini/meta-cassini@ad0cda4193d2a07e8fa8b940b91e08f9a64021b8) ([merge request](Linaro/cassini/meta-cassini!101))
- [ci: Ensure N1SDP ACS results are available](Linaro/cassini/meta-cassini@a8fcf4e20cc6d24ffadde188585a86ac0635b110) ([merge request](Linaro/cassini/meta-cassini!92))

### feature (4 changes)

- [ci: Adopt v2 of the Corstone-1000 FPGA image](Linaro/cassini/meta-cassini@eb2064682fdf0c598ac03632a146d9518cb48821) ([merge request](Linaro/cassini/meta-cassini!101))

## 1.0.1 (2023-10-31)

### other (3 changes)

- [cassini-[doc,config,distro]: Cassini 1.0.1 release](Linaro/cassini/meta-cassini@289ce064225d8da582e04dc301cb8674baeb8730)
- [cassini-config: Don't unpin upstream refspec for dev images](Linaro/cassini/meta-cassini@89bac3cb3245a0b925b28c8253325674dca2b79f)
- [ci: Disable headercheck (copyright/license)](Linaro/cassini/meta-cassini@fe0be77ebda4da988d09bdddecd8261fabd987bc)

### bug (1 change)

- [cassini-distro: Switch from `docker-ce` to `docker-moby`](Linaro/cassini/meta-cassini@ed0ce973ee4850ac4a63244c6fda9b9e9b36b15c)

### security (2 changes)

- [cassini-[config,doc]: Update meta-virtualization to fix docker](Linaro/cassini/meta-cassini@ed6633dfb04603d29a607cef9bc000afa8e428f6)
- [cassini-distro: Disable Parsec detailed error trace](Linaro/cassini/meta-cassini@9a34d83388a672e0e63e02075fd01e681d818103)

## 1.0.0 (2023-08-21)

### other (15 changes)

- [cassini-[doc,config,distro]: Cassini 1.0.0 release](Linaro/cassini/meta-cassini@917bc94fe71459d33cba2a3cbf8fa0206c0d6dcc)
- [docs: Add note that K3S is not supported on Corstone-1000](Linaro/cassini/meta-cassini@c0b502c4b38182ad5ad7f05c812ef510d302540f) ([merge request](Linaro/cassini/meta-cassini!69))
- [ci: Update to newer 1.5.1 pipeline template version](Linaro/cassini/meta-cassini@ad851b52c7855307c89f23da06dc4e6ed0144be5) ([merge request](Linaro/cassini/meta-cassini!46))
- [ci: Add output of jfrog upload log to test runs](Linaro/cassini/meta-cassini@819836cf48fd40e68d64fb9ceb24c6c2518a9640) ([merge request](Linaro/cassini/meta-cassini!46))
- [ci: Filter out test jobs if unable to run them under lava](Linaro/cassini/meta-cassini@02f5d30eba731723018c5a3105e8dceb2e0012d2) ([merge request](Linaro/cassini/meta-cassini!34))
- [ci: Switch to the cspell checker](Linaro/cassini/meta-cassini@a4cc4054f13998869810824583d9ef2cefaf5315) ([merge request](Linaro/cassini/meta-cassini!14))
- [doc: Move platform specific instructions to `Getting Started` guides](Linaro/cassini/meta-cassini@1a90f73092f2e3fb85d849a1d55a45efcb181fc8)
- [doc: Expand commit guidelines](Linaro/cassini/meta-cassini@4fe6634f59b81e1cf3463c5bc27a3681a6226311)
- [ci: Run Corstone-1000 FVP sanity test in lava farm](Linaro/cassini/meta-cassini@0f924cdfc165ae861f6c3219d85538fe246626cc)
- [[cassini-bsp, cassini-distro] Move platform specific settings](Linaro/cassini/meta-cassini@df47a58857634315074fb3cc9166c1d0616b375d)
- [cassini-distro: Explicitly set DISTRO_FEATURES](Linaro/cassini/meta-cassini@eadb77e2756aed02886a336ff197e3d0c0ebd626)
- [ci: Move to new template version v1.1.1](Linaro/cassini/meta-cassini@45922037eb71a3a072ec6848490cc0063391d42e)
- [ci: Add ACS test suite running](Linaro/cassini/meta-cassini@68f2ffb343db4970ab508f80ecbb12cb81aec4cc)
- [ci: Switch to using GitLab Code Quailty for qa_checks](Linaro/cassini/meta-cassini@039b439de6e8110f842593dedb226e6a53eb1ad9)
- [ci: Get lava results of test runs](Linaro/cassini/meta-cassini@0db0595e632678e7a995998855b39f19d28b1140)

### bug (15 changes)

- [cassini-bsp: Re-enable CGROUPS for docker (Corstone-1000)](Linaro/cassini/meta-cassini@e725985939fba2260b25170d58b69d84d486cbed) ([merge request](Linaro/cassini/meta-cassini!87))
- [cassini-bsp: Fix issues loading kernel modules on Corstone-1000](Linaro/cassini/meta-cassini@7893f800c366458ae58a5c216d44ca41e766f655) ([merge request](Linaro/cassini/meta-cassini!87))
- [meta-cassini-bsp: Always build the utilities image for corstone1000-mps3](Linaro/cassini/meta-cassini@ec27a74e1cdf7d257295dd0bd2be6673ec01fb13) ([merge request](Linaro/cassini/meta-cassini!46))
- [ci: Drop TLS use when uploading test results](Linaro/cassini/meta-cassini@c4899ebf48f03c3a66edbd00a220833e0b9d5784) ([merge request](Linaro/cassini/meta-cassini!46))
- [cassini-bsp: Reserve memory for SE comm on Corstone-1000](Linaro/cassini/meta-cassini@1a68841f3f1fe7be5816abe624608a407608f6e0) ([merge request](Linaro/cassini/meta-cassini!37))
- [ci: Modify lava tests scripts for Corstone-1000 MPS3](Linaro/cassini/meta-cassini@df46057d25496a07ccc6e633770099d13707e3a7) ([merge request](Linaro/cassini/meta-cassini!33))
- [distro: Fix cassini-dev distro feature builds](Linaro/cassini/meta-cassini@b775a7fe17eb60bd0b58ac242f27339a20077a6d)
- [bsp: Increase Corstone-1000 systemd device timeout](Linaro/cassini/meta-cassini@f2a25dd9b36fdc137c5a35f682a4898ecf433985)
- [bsp: Fix Corstone-1000 build configuration](Linaro/cassini/meta-cassini@8135bc7c4af753d24cef81293d5b5c7014971781)
- [Use v1.4.2 version of the templates](Linaro/cassini/meta-cassini@b722b38291064166e31a056157db874412c53fec)
- [ci: Use CI_REGISTRY_IMAGE in image paths](Linaro/cassini/meta-cassini@ae53147c5d8c8c472b1ef825d3c7ad597240f671)
- [ci: Workaround jfrog cli issue](Linaro/cassini/meta-cassini@7a0702a9b84eef18661b801bdd9097e61a1a6af0)
- [ci: Base kas image on latest-release version](Linaro/cassini/meta-cassini@3cc96e254fda1ea739a7a6a8ac3c66cb791a94f6)
- [[bsp,distro,test] Update layer compatibility](Linaro/cassini/meta-cassini@325da77bd0fb15ab201b67e5f7789cf7c2c6f805)
- [cassini-bsp/meta-arm: Add QSPI support to N1SDP edk2 build](Linaro/cassini/meta-cassini@f423c856416f4ce2c55a209739f0af9f9755772c)

### removed (1 change)

- [cassini-bsp: Remove K3S from Corstone-1000](Linaro/cassini/meta-cassini@907ca9491ea7f7fa042c0c405810e6f44b28d170) ([merge request](Linaro/cassini/meta-cassini!69))

### feature (7 changes)

- [cassini-bsp: Enable Block Storage Service on N1SDP](Linaro/cassini/meta-cassini@c9be323995f537d8c188ddecfaa5357d3cd7411e) ([merge request](Linaro/cassini/meta-cassini!21))
- [ci: Add acs testing to Corstone-1000 FVP](Linaro/cassini/meta-cassini@f4e1b9d2176e59d91372257191a6ccfff1ef3c22) ([merge request](Linaro/cassini/meta-cassini!19))
- [meta-cassini-config: Add kas menu configuration file](Linaro/cassini/meta-cassini@97bfb77ab0765b5426542a070b571da44e288d23) ([merge request](Linaro/cassini/meta-cassini!18))
- [[cassini-bsp, cassini-config] Add Corstone-1000 FVP to build](Linaro/cassini/meta-cassini@140620b5a1d2d70a75bb04c967e960e837bb1e95)
- [cassini-[docs,bsp,config,distro]: Add Corstone-1000-mps3 board support](Linaro/cassini/meta-cassini@fc039807910db227000d287a65604ea51f00f2cc)
- [meta-cassini-bsp/conf: Add support to build & install secure partitions](Linaro/cassini/meta-cassini@13fa88a3add002824b444ea23c70d75ef0696579)
- [cassini-[doc,config,distro,tests]: Add PARSEC support](Linaro/cassini/meta-cassini@e2cc34a7971b947f384379fa3d61244e42cb7751)

## 0.9.0 (2022-08-15)

No changes.

## 0.0.1 (2022-07-01)

No changes.
