<!--
# SPDX-FileCopyrightText: Copyright (c) 2024, Linaro Limited.
#
# SPDX-FileCopyrightText: <text>Copyright 2022-2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT
-->

# Project Cassini

Project Cassini is the open, collaborative, standards-based initiative to
deliver a seamless cloud-native software experience for devices based on Arm
Cortex-A.

The project documentation can be found at:
https://cassini.readthedocs.io/en/latest/

## Repository License

The repository's standard license is the MIT license, under which most of the
repository's content is provided. Exceptions to this standard license relate to
files that represent modifications to externally licensed works (for example,
patch files). These files may therefore be included in the repository under
alternative licenses in order to be compliant with the licensing requirements of
the associated external works.

License details may be found in the [local license file](LICENSE.rst), or as
part of the project documentation.

Contributions to the project should follow the same licensing arrangement.

## Contact

Please see the project documentation for the list of maintainers, as well as the
process for submitting contributions, raising issues, or receiving feedback and
support.
