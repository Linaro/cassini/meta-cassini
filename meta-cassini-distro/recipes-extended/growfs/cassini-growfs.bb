# SPDX-FileCopyrightText: Copyright (c) 2024, Linaro Limited.
#
# SPDX-License-Identifier: MIT

SUMMARY = "GrowFs and Repart Helper"
DESCRIPTION = "Helps to repartitioning and growing file system \
    during boot-time"
HOMEPAGE = "https://cassini.readthedocs.io/en/latest/"
LICENSE = "MIT"

# nooelint: oelint.var.licenseremotefile
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = "file://50-root.conf \
           file://override.conf"

S = "${WORKDIR}/home"
UNPACKDIR = "${S}"

inherit allarch features_check

REQUIRED_DISTRO_FEATURES = "systemd"

do_install(){

    install -d ${D}${nonarch_libdir}/repart.d/
    install -m  0644 ${UNPACKDIR}/50-root.conf ${D}${nonarch_libdir}/repart.d/50-root.conf

    install -d ${D}${systemd_system_unitdir}/systemd-repart.service.d/
    install -m 0644 ${UNPACKDIR}/override.conf ${D}${systemd_system_unitdir}/systemd-repart.service.d/override.conf

}

PACKAGES = "${PN}"
FILES:${PN} += "${nonarch_libdir}/repart.d/50-root.conf \
    ${systemd_system_unitdir}/systemd-repart.service.d/override.conf "

