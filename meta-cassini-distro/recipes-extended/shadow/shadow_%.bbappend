# SPDX-FileCopyrightText: <text>Copyright 2022-2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

do_install:append:cassini() {

    # Make sure that users cannot access to each other HOME directory
    sed -i 's/#HOME_MODE/HOME_MODE/g' ${D}${sysconfdir}/login.defs
}

CASSINI_SECURITY_UMASK ??= "0027"

# nooelint: oelint.func.specific - Distro feature name
do_install:append:cassini-security() {
    # set more secure UMASK
    sed -i "s/UMASK.*/UMASK\t\t${CASSINI_SECURITY_UMASK}/g" \
        ${D}${sysconfdir}/login.defs
}
