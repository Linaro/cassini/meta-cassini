# SPDX-FileCopyrightText: <text>Copyright 2022-2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-FileCopyrightText: Copyright (c) 2024, Linaro Limited.
#
# SPDX-License-Identifier: MIT

FILESEXTRAPATHS:prepend:cassini := "${THISDIR}/files:"

QUIET_PRINTK = "20-quiet-printk.conf"

SRC_URI:append:cassini = "file://${QUIET_PRINTK}"

do_install:append:cassini() {
    install -Dm 0640 ${UNPACKDIR}/${QUIET_PRINTK} \
        ${D}${sysconfdir}/sysctl.d/${QUIET_PRINTK}
}
