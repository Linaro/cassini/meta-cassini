# SPDX-FileCopyrightText: Copyright (c) 2025, Linaro Limited.
#
# SPDX-License-Identifier: MIT

FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI:append:cassini = " file://00_mender_default_grub.cfg;subdir=git "

SRC_URI:append:cassini:zynqmp-kria-starter-psa = " file://01_mender_zynqmp_grub.cfg;subdir=git "
