# SPDX-FileCopyrightText: Copyright (c) 2024-2025, Linaro Limited.
#
# SPDX-License-Identifier: MIT

PACKAGE_INSTALL:append = " \
    ${@bb.utils.contains('DISTRO_FEATURES', 'systemd', 'udev', 'eudev', d)} \
    ${@bb.utils.contains('DISTRO_FEATURES', 'sota', "ostree-switchroot", "", d)} \
    ${ROOTFS_BOOTSTRAP_INSTALL} \
    ${VIRTUAL-RUNTIME_base-utils} \
    base-passwd \
    coreutils \
    dbus \
    e2fsprogs-mke2fs \
    efivar \
    kmod \
    mmc-utils \
    os-release-initrd \
    rng-tools \
    smartmontools \
    util-linux-blkid \
    util-linux-mount \
    util-linux-umount \
"
# Remove cloud-service and ota-update
IMAGE_FEATURES:remove = "cloud-service ota-update"
