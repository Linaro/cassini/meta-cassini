# SPDX-FileCopyrightText: Copyright (c) 2024-2025, Linaro Limited.
#
# SPDX-License-Identifier: MIT

# nooelint: oelint.vars.downloadfilename
SRC_URI:prepend = "https://d2s8p88vqu9w66.cloudfront.net/releases/greengrass-2.12.0.zip;name=payload; "

SRC_URI[payload.sha256sum] = "3f3cb3f9c8a254c7c9fb559890347da2f43c8cb22c1eed9f3eb548604104245d"

SRC_URI:remove = "https://d2s8p88vqu9w66.cloudfront.net/releases/greengrass-${PV}.zip;name=payload; \
                  file://001-serviced-startup-sh.patch"

RDEPENDS:${PN} += "python3-pip"

