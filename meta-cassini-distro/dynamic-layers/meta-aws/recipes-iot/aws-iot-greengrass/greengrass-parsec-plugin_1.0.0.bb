# SPDX-FileCopyrightText: Copyright (c) 2024, Linaro Limited.
#
# SPDX-License-Identifier: MIT

SUMMARY = "AWS Greengrass Crypto Parsec Provider"
DESCRIPTION = "Install Java based AWS Greengrass crypto parsec provider plugin"
HOMEPAGE = "https://cassini.readthedocs.io/en/latest/"
LICENSE = "MIT"

# nooelint: oelint.var.licenseremotefile
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

GG_HOME = "/greengrass/v2"
PLUGIN_NAME = "aws.greengrass.crypto.ParsecProvider.jar"

SRC_URI = "https://gitlab.com/api/v4/projects/44378631/packages/generic/greengrass-parsec-plugin/${PV}/${PLUGIN_NAME};unpack=false"

SRC_URI[sha256sum] = "fe34a4e6084b1002809d06f675524049b58b723ddbe8cccde31bb9c02fdf42fb"

inherit allarch

UNPACKDIR = "${S}"

FILES:${PN} += "${GG_HOME}/${PLUGIN_NAME}"

do_install() {
    install -d "${D}/${GG_HOME}"
    install --mode="755" "${UNPACKDIR}/${PLUGIN_NAME}" "${D}/${GG_HOME}/${PLUGIN_NAME}"
}
