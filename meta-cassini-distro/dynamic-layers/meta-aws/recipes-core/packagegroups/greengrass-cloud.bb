# SPDX-FileCopyrightText: <text>Copyright 2024 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

SUMMARY = "Greengrass Cloud Integration"
DESCRIPTION = "Include Greengrass related cloud integration"
LICENSE = "MIT"
SRC_URI = ""

inherit packagegroup

RDEPENDS:${PN} += "\
    ${@bb.utils.contains('DISTRO_FEATURES',\
                         'cassini-test', "${PN}-ptest", '', d)} \
    ${VIRTUAL-RUNTIME_container_engine} \
    greengrass-bin \
"
