# SPDX-FileCopyrightText: Copyright (c) 2025, Linaro Limited.
#
# SPDX-License-Identifier: MIT

SUMMARY = "No OTA update Integration"
DESCRIPTION = "Dummy recipe when no Over-the-air update is required"
LICENSE = "MIT"
SRC_URI = ""

inherit packagegroup

RDEPENDS:${PN} = "\
    ${@bb.utils.contains('DISTRO_FEATURES',\
                         'cassini-test', "${PN}-ptest", '', d)} \
"
