# SPDX-FileCopyrightText: Copyright (c) 2025, Linaro Limited.
#
# SPDX-License-Identifier: MIT

SUMMARY = "Mender OTA update integration"
DESCRIPTION = "Include Mender Over-the-air update integration"
LICENSE = "MIT"
SRC_URI = ""

inherit packagegroup

RDEPENDS:${PN} += "\
    ${@bb.utils.contains('DISTRO_FEATURES',\
                         'cassini-test', "${PN}-ptest", '', d)} \
"
