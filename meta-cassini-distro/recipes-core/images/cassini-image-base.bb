# nooelint: oelint.var.mandatoryvar
#
# SPDX-FileCopyrightText: Copyright (c) 2024, Linaro Limited.
#
# SPDX-FileCopyrightText: <text>Copyright 2022-2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

SUMMARY = "CASSINI base image core config with common packages"

IMAGE_LINGUAS = ""

LICENSE = "MIT"

inherit cassini-image

# Enable EFI booting configurations for genericarm64 image
inherit ${@bb.utils.contains("MACHINE", "genericarm64", "image-efi-boot-genericarm64", "", d)}
