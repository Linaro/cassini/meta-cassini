# SPDX-FileCopyrightText: <text>Copyright 2022-2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-FileCopyrightText: Copyright (c) 2024, Linaro Limited.
#
# SPDX-License-Identifier: MIT

FILESEXTRAPATHS:prepend:cassini := "${THISDIR}/files:"

SRC_URI:append:cassini = " file://cassini_profile.sh"

CASSINI_SECURITY_UMASK ??= "0027"

do_install:append:cassini() {
    # PS1 is set inside cassini_profile.sh
    sed -i '/PS1/d' ${D}${sysconfdir}/skel/.bashrc

    install -d ${D}${sysconfdir}/profile.d

    # Others are given 'read' permission so that profile env vars are passed
    # through to other user accounts correctly
    install -m 0644 ${UNPACKDIR}/cassini_profile.sh \
        ${D}${sysconfdir}/profile.d/cassini_profile.sh
}

# nooelint: oelint.func.specific - Distro feature name
do_install:append:cassini-security() {
    # set more secure umask
    sed -i "s/umask.*/umask ${CASSINI_SECURITY_UMASK}/g" \
        ${D}${sysconfdir}/profile

    sed -i "s/umask.*/umask ${CASSINI_SECURITY_UMASK}/g" \
        ${D}${sysconfdir}/skel/.bashrc
}
