# SPDX-FileCopyrightText: Copyright (c) 2024, Linaro Limited.
#
# SPDX-FileCopyrightText: <text>Copyright 2024 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

DISTRO_FEATURES:append:cassini = " k3s"

# These classes produce warnings if there are any missing kernel configurations
# that are required by their target packages
KERNEL_CLASSES:append = " k3s_kernelcfg_check"

# meta-virtualization/recipes-containers/k3s/README.md states that K3s requires
# 2GB of space in the rootfs to ensure containers can start
# nooelint: oelint.vars.mispell - Addition of new variable
CASSINI_ROOTFS_EXTRA_SPACE ?= "2000000"

