# SPDX-FileCopyrightText: Copyright (c) 2025, Linaro Limited.
#
# SPDX-License-Identifier: MIT

VIRTUAL-RUNTIME_ota_update ??= "mender-ota"
IMAGE_FEATURES:append:cassini = " ota-update"

include ${@bb.utils.contains(\
'VIRTUAL-RUNTIME_ota_update','mender-ota', \
'ota-update/mender.inc', '', d)}
