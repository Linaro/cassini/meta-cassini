# SPDX-FileCopyrightText: Copyright (c) 2025, Linaro Limited.
#
# SPDX-License-Identifier: MIT

#--- mender config ---
MENDER_ARTIFACT_NAME = "release-1"
IMAGE_FSTYPES:remove:cassini = "wic wic.gz wic.bmap"
IMAGE_FSTYPES:append:cassini = " uefiimg.gz"
ARTIFACTIMG_FSTYPE:cassini = "ext4"

# On ARM, U-Boot is used as a first stage bootloader which provides a UEFI loader,
# and this is then used to boot GRUB using the UEFI boot standard
# Since the kernel-devicetree is already provided, we don't want to include it
MACHINE_ESSENTIAL_EXTRA_RDEPENDS:remove:mender-image:aarch64 = "kernel-devicetree"

# Use PARTUUID to set fixed drive locations
MENDER_FEATURES_ENABLE:append:cassini = " mender-partuuid"

# UUID's generated using the 'uuidgen -r' command
MENDER_BOOT_PART = "/dev/disk/by-partuuid/d3deac4d-9e08-4b19-87bb-b4c94b8cea28"
MENDER_ROOTFS_PART_A = "/dev/disk/by-partuuid/14204b13-610a-4dfa-8a85-99810a3251bd"
MENDER_ROOTFS_PART_B = "/dev/disk/by-partuuid/c6e8ed73-1ee2-4ac4-bfba-058ee658ea26"
MENDER_DATA_PART = "/dev/disk/by-partuuid/c43fb879-d685-4b84-b74f-fbd5d8cdf195"

#  --- mender-image ---
# Cassini distro without any cloud service requires at least 3GB.
# If there is any cloud service or cassini-test added to the distro
# then we require extra space, due to CASSINI_ROOTFS_EXTRA_SPACE being set.
# Mender uses dual partition system for rootfs and uses
# MENDER_STORAGE_TOTAL_SIZE_MB to set the size of storage medium to calculate
# layout with each partition. It is not recommended to set
# IMAGE_ROOTFS_EXTRA_SPACE explicitly, which can be used to define a single
# partition size, because this variable is used by other components
# Therefore extra 5GB is added to MENDER_STORAGE_TOTAL_SIZE_MB

IMAGE_OVERHEAD_FACTOR = "1.3"
MENDER_STORAGE_TOTAL_SIZE_MB = "${@ int(3072 + \
      (5120 if d.getVar('VIRTUAL-RUNTIME_cloud_service') != 'no-cloud' \
      or bb.utils.contains('DISTRO_FEATURES', 'cassini-test', True, False, d) \
      else 0)) }"


# --- mender-client ---
# The version of Mender to build. This needs to match an existing recipe in the meta-mender repository.
#
# Given your Yocto Project version, see which versions of Mender you can currently build here:
# https://docs.mender.io/overview/compatibility#mender-client-and-yocto-project-version
#
# Given a Mender client version, see the corresponding version of the mender-artifact utility:
# https://docs.mender.io/overview/compatibility#mender-clientserver-and-artifact-format
#
PREFERRED_VERSION_mender = "4.0.4"
PREFERRED_PROVIDER_mender-native = "mender-native"
PREFERRED_RPROVIDER_mender-auth = "mender"
PREFERRED_RPROVIDER_mender-update = "mender"

# EFI_LOADER configuration
MENDER_EFI_LOADER:zynqmp-kria-starter-psa = "${PREFERRED_PROVIDER_virtual/bootloader}"

# Genericarm64 configuration
_MENDER_PART_IMAGE_DEPENDS:remove:genericarm64 = " wic-tools:do_populate_sysroot"

# Enabling the kernel config for arasan mmc driver in the kernel for
# genericarm64 mender build so kv260 can boot the mender image without
# initramfs image
KERNEL_EXTRA_ARGS:genericarm64:append = "${@bb.utils.contains(\
    'VIRTUAL-RUNTIME_ota_update', 'mender-ota', 'CONFIG_MMC_SDHCI_OF_ARASAN=y', '', d)}"
