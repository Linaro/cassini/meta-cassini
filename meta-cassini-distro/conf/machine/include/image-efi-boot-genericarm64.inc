# SPDX-FileCopyrightText: Copyright (c) 2024, Linaro Limited.
#
# SPDX-License-Identifier: MIT

IMAGE_EFI_BOOT_FILES += "core-image-initramfs-boot-${MACHINE}.cpio.gz"

ROOT_PART_UUID = "f3374295-b635-44af-90b6-3f65ded2e2e4"
ROOT_FS_UUID = "6091b3a4-ce08-3020-93a6-f755a22ef03b"
WKS_ROOTFS_PART_EXTRA_ARGS = "--uuid=${ROOT_PART_UUID} --fsuuid=${ROOT_FS_UUID}"

GRUB_CFG_FILE = "${WORKDIR}/grub.cfg"

do_image_wic[prefuncs] += "generate_grub_cfg"

generate_grub_cfg() {
	cat <<-'EOF' > "${GRUB_CFG_FILE}"
	set term="vt100"
	set default="0"
	set timeout="5"

	kernel_cmdline="rootwait rw panic=60"

	menuentry '${DISTRO_NAME}' {
	        echo 'Loading Linux ...'
	        linux /${KERNEL_IMAGETYPE} $kernel_cmdline root=UUID=@ROOT_FS_UUID@ @OSTREE_ARG@
	        echo 'Loading initial ramdisk ...'
	        initrd /core-image-initramfs-boot-${MACHINE}.cpio.gz
	}
	EOF

	# Replace placeholders in the generated grub config
	sed -i -e "s|@ROOT_FS_UUID@|${ROOT_FS_UUID}|" \
			-e "s|@OSTREE_ARG@|${OSTREE_ARG}|" \
			${GRUB_CFG_FILE}
}
