# SPDX-FileCopyrightText: Copyright (c) 2024, Linaro Limited.
#
# SPDX-FileCopyrightText: <text>Copyright 2022 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

inherit kernelcfg_check

# Current checksum, should be updated to track latest containers requirements
# https://git.yoctoproject.org/yocto-kernel-cache/tree/cfg/docker.cfg
CONTAINERS_CONFIG_FILE ?= "docker.cfg"
CONTAINERS_CONFIG_FILE_MD5 ?= "5571c9fdd93941304e44aeb2189203ce"

python do_containers_kernelcfg_check() {
    kernelcfg_check(d, \
                    d.getVar('CONTAINERS_CONFIG_FILE'), \
                    d.getVar('CONTAINERS_CONFIG_FILE_MD5'))
}

addtask containers_kernelcfg_check before do_compile after do_configure
