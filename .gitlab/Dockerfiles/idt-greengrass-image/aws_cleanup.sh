#!/bin/bash
#
# SPDX-FileCopyrightText: Copyright (c) 2024, Linaro Limited.
#
# SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT
#
# The AWS CLI should be setup with credentials and configured to run this
# script with the following parameters:
# - AWS_ACCESS_KEY_ID
# - AWS_SECRET_ACCESS_KEY
# - AWS_DEFAULT_REGION
#
# Required Parameters:
# - IOT_THING_NAME

set -e

# ---------------------------------------------
# Help Function
# ---------------------------------------------
Help()
{
  error_flag=1
  if [[ -z "$IOT_THING_NAME" ]]; then
    echo "IOT_THING_NAME must be set in environment" 1>&2
  else
    error_flag=0
  fi
  return $error_flag
}

# ---------------------------------------------
# Main Script
# ---------------------------------------------
Help

FORCE_S3_BUCKET_CLEANUP=${FORCE_S3_BUCKET_CLEANUP:-false}

# Delete Thing & Certificates
echo "Deleting ${IOT_THING_NAME} thing and certificates if exist"
if aws iot describe-thing --thing-name "${IOT_THING_NAME}" 2>/dev/null 1>&2; then
  # Delete old certificates if exist
  echo "Deleting certificates"
  for cert in $(aws iot list-thing-principals --thing-name "${IOT_THING_NAME}" --query "principals" --output text); do
    aws iot detach-thing-principal --thing-name "${IOT_THING_NAME}" --principal "${cert}"

    cert_id=${cert##*/}
    aws iot update-certificate --certificate-id "${cert_id}" --new-status "INACTIVE"
    aws iot delete-certificate --force-delete --certificate-id "${cert_id}"
  done

  if aws greengrassv2 get-core-device --core-device-thing-name "${IOT_THING_NAME}" 2>/dev/null 1>&2; then
    echo "Deleting the core device for ${IOT_THING_NAME}"
    aws greengrassv2 delete-core-device --core-device-thing-name "${IOT_THING_NAME}"
  fi

  echo "Remove ${IOT_THING_NAME} from groups"
  readarray -t group_name < <(aws iot list-thing-groups-for-thing --thing-name "${IOT_THING_NAME}" | jq -r '.thingGroups[].groupName')
  for each in "${group_name[@]}"
  do
    aws iot remove-thing-from-thing-group --thing-name "${IOT_THING_NAME}" --thing-group-name "${each}"
  done

  echo "Deleting the thing"
  aws iot delete-thing --thing-name "${IOT_THING_NAME}"
  echo "${IOT_THING_NAME} has been deleted"
else
  echo "${IOT_THING_NAME} thing is not found"
fi

# List S3 buckets which were created by IoT device tester
echo "Pending S3 buckets awaiting cleanup..."
aws s3 ls | grep "idt-*" > /tester/scripts/idt-s3-buckets.txt || true
cat /tester/scripts/idt-s3-buckets.txt

# Force delete S3 buckets which are older than 30 days
if [[ $FORCE_S3_BUCKET_CLEANUP == "true" ]]; then
  while IFS= read -r line; do
    #Parsing format: 2024-07-23 03:52:04 idt-36455ebd8d108fb29d8c-gg-component-store
    thirty_days_ago=$(date +%F -d '30 days ago')
    date_in_file=$(echo "${line}" | cut -d' ' -f1 -)
    if [[ $date_in_file < $thirty_days_ago ]]; then
      uri="s3://$(echo "${line}" | cut -d' ' -f3 -)"
      echo "Deleting: ${uri}"
      aws s3 rb "${uri}" --force
    fi
  done < "/tester/scripts/idt-s3-buckets.txt"

  # List remaining S3 buckets
  echo "Remaining S3 buckets after cleanup..."
  aws s3 ls | grep "idt-*"
fi
