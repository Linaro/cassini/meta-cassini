#!/bin/bash
#
# SPDX-FileCopyrightText: Copyright (c) 2024, Linaro Limited.
#
# SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT
#
# The AWS CLI should be setup with credentials and configured to run this
# script with the following parameters:
# - AWS_ACCESS_KEY_ID
# - AWS_SECRET_ACCESS_KEY
# - AWS_DEFAULT_REGION
#
# Manually provision an AWS GreenGrass IOT thing, this script does the
# following:
# - Create IoT Thing & Group
# - Use the CSR from Parsec (this file needs to be present)
# - Setup the thing with required role
# - Generates GreenGrass config file
# The script follows https://docs.aws.amazon.com/greengrass/v2/developerguide/manual-installation.html
#
# Required Parameters:
# - IOT_THING_NAME
# - IOT_THING_GROUP
# - IOT_THING_POLICY
# - IOT_TE_ROLE_POLICY_ALIAS
# - IOT_TE_ROLE_ALIAS
# - IOT_KEY_NAME

set -e

# ---------------------------------------------
# Help Function
# ---------------------------------------------
Help()
{
  error_flag=1
  if [[ -z "$IOT_THING_NAME" ]]; then
    echo "IOT_THING_NAME must be set in environment" 1>&2
  elif [[ -z "$IOT_THING_GROUP" ]]; then
    echo "IOT_THING_GROUP must be set in environment" 1>&2
  elif [[ -z "$IOT_THING_POLICY" ]]; then
    echo "IOT_THING_POLICY must be set in environment" 1>&2
  elif [[ -z "$IOT_TE_ROLE_POLICY_ALIAS" ]]; then
    echo "IOT_TE_ROLE_POLICY_ALIAS must be set in environment" 1>&2
  elif [[ -z "$IOT_TE_ROLE_ALIAS" ]]; then
    echo "IOT_TE_ROLE_ALIAS must be set in environment" 1>&2
  else
    error_flag=0
  fi
  return $error_flag
}

# ---------------------------------------------
# Main Script
# ---------------------------------------------
Help

GG_HOME=${GG_HOME:-/greengrass/v2}

mkdir -p "${GG_HOME}"

AWS_ID=$(aws sts get-caller-identity --query "Account" --output text)

if [ -z "$AWS_ID" ]; then
  echo "Can't get AWS Account ID. Check that:"
  echo " - aws cli tool installed and"
  echo " - AWS_ACCESS_* env variables have not expired"
  exit 1
fi

if [ -z "$AWS_DEFAULT_REGION" ]; then
  AWS_DEFAULT_REGION=$(aws configure get region --output text)
fi

# ---------------------------------------------
# Clean Up
# ---------------------------------------------

# shellcheck source=/dev/null
source /tester/scripts/aws_cleanup.sh

# ---------------------------------------------
# Create Thing & Group
# ---------------------------------------------

# https://docs.aws.amazon.com/greengrass/v2/developerguide/manual-installation.html#create-iot-thing
echo "Creating ${IOT_THING_NAME} IoT thing"

aws iot create-thing --thing-name "${IOT_THING_NAME}" >/dev/null

aws iot add-thing-to-thing-group --thing-name "${IOT_THING_NAME}" --thing-group-name "${IOT_THING_GROUP}"

# https://docs.aws.amazon.com/greengrass/v2/developerguide/manual-installation.html#create-thing-certificate
echo "Create the thing certificate using an RSA key created with parsec-tool"

# Create a new certificate using the CSR from Parsec
gg_cert_arn=$(aws iot create-certificate-from-csr --set-as-active \
        --certificate-signing-request="file://${GG_HOME}/${IOT_KEY_NAME}.csr" \
        --certificate-pem-outfile "${GG_HOME}/device.pem.crt" \
        --output text --query "certificateArn")

# https://docs.aws.amazon.com/greengrass/v2/developerguide/manual-installation.html#configure-thing-certificate
if ! aws iot get-policy --policy-name "${IOT_THING_POLICY}" 2>/dev/null 1>&2; then
  echo "${IOT_THING_POLICY} thing policy not defined"
  exit 1
fi

echo "Attaching target to ${IOT_THING_POLICY}"
aws iot attach-policy --policy-name "${IOT_THING_POLICY}" --target "${gg_cert_arn}"

# Verify policy attachment for the IOT target.
echo "Checking ${IOT_THING_POLICY} policy attachment for target"
readarray -t VERIFY_IOT_POLICY_ATTACHMENT < <(aws iot list-targets-for-policy --policy-name "${IOT_THING_POLICY}" | jq -r '.targets[]')
verify=false
for each in "${VERIFY_IOT_POLICY_ATTACHMENT[@]}"
do
  if [[ "$gg_cert_arn" == "$each" ]]; then
    verify=true
    break
  fi
done

if [[ $verify == false ]]; then
  echo "IOT - target - Verification failed for policy attachment"
  exit 1
fi

echo "Attaching certificate to IoT thing"
aws iot attach-thing-principal --thing-name "${IOT_THING_NAME}" --principal "${gg_cert_arn}"

# Verify certificate attachment for the IOT thing.
echo "Checking ${IOT_THING_NAME} principal attachment"
readarray -t VERIFY_THING_PRINCIPAL_ATTACHMENT < <(aws iot list-thing-principals --thing-name "${IOT_THING_NAME}" | jq -r '.principals[]')
for each in "${VERIFY_THING_PRINCIPAL_ATTACHMENT[@]}"
do
  if [[ "$gg_cert_arn" != "$each" ]]; then
    echo "IOT - thing - Verification failed for principal attachment"
    exit 1
  fi
done

if ! aws iot describe-role-alias --role-alias "${IOT_TE_ROLE_ALIAS}" 2>/dev/null 1>&2; then
  echo "${IOT_TE_ROLE_ALIAS} not found"
  exit 1
fi

echo "Attaching IoT role alias policy to certificate"
aws iot attach-policy --policy-name "${IOT_TE_ROLE_POLICY_ALIAS}" --target "${gg_cert_arn}"

# Verify TE policy attachment for the IOT target.
echo "Checking ${IOT_TE_ROLE_POLICY_ALIAS} policy attachment for target"
readarray -t VERIFY_IOT_POLICY_ATTACHMENT < <(aws iot list-targets-for-policy --policy-name "${IOT_TE_ROLE_POLICY_ALIAS}" | jq -r '.targets[]')
verify=false
for each in "${VERIFY_IOT_POLICY_ATTACHMENT[@]}"
do
  if [[ "$gg_cert_arn" == "$each" ]]; then
    verify=true
    break
  fi
done

if [[ $verify == false ]]; then
  echo "IOT - target - Verification failed for TE alias policy attachment"
  exit 1
fi

echo "Creating GreenGrass config file"
iot_endpoint=$(aws iot describe-endpoint --endpoint-type "iot:Data-ATS" --output text)
cred_endpoint=$(aws iot describe-endpoint --endpoint-type "iot:CredentialProvider" --output text)

wget https://www.amazontrust.com/repository/AmazonRootCA1.pem -O "${GG_HOME}/AmazonRootCA1.pem" 2>/dev/null

cat <<EOF >"${GG_HOME}/generated_config.yml"
system:
  certificateFilePath: "parsec:import=${GG_HOME}/device.pem.crt;object=${IOT_KEY_NAME};type=cert"
  privateKeyPath: "parsec:object=${IOT_KEY_NAME};type=private"
  rootCaPath: "${GG_HOME}/AmazonRootCA1.pem"
  rootpath: "${GG_HOME}"
  thingName: "${IOT_THING_NAME}"
services:
  aws.greengrass.Nucleus:
    componentType: "NUCLEUS"
    configuration:
      awsRegion: "${AWS_DEFAULT_REGION}"
      iotRoleAlias: "${IOT_TE_ROLE_ALIAS}"
      iotDataEndpoint: "${iot_endpoint}"
      iotCredEndpoint: "${cred_endpoint}"
      logging:
        level: "INFO"
  aws.greengrass.crypto.ParsecProvider:
    configuration:
      name: "aws.greengrass.crypto.ParsecProvider"
      parsecSocket: "/run/parsec/parsec.sock"
EOF

echo "GreenGrass configuration file ${GG_HOME}/generated_config.yml has been generated"

echo "Waiting until the role and the policy are functional"
while ! aws iot describe-thing --thing-name "${IOT_THING_NAME}" 2>/dev/null; do
  sleep 2s
done
