#!/bin/bash
#
# SPDX-FileCopyrightText: Copyright (c) 2024, Linaro Limited.
#
# SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT
#
# Generate device.json that is needed by IDT
#
# Required Parameters:
# - THING_IP
# - TARGET_MACHINE
# - TARGET_PORT - Optional (default to 22)

set -e

# ---------------------------------------------
# Help Function
# ---------------------------------------------
Help()
{
  error_flag=1
  if [[ -z "$THING_IP" ]]; then
    echo "THING_IP must be set in environment" 1>&2
  elif [[ -z "$TARGET_MACHINE" ]]; then
    echo "TARGET_MACHINE must be set in environment" 1>&2
  else
    error_flag=0
  fi
  return $error_flag
}

# ---------------------------------------------
# Main Script
# ---------------------------------------------
Help

# ---------------------------------------------
# Create Device Configuration
# ---------------------------------------------

TARGET_PORT=${TARGET_PORT:-22}

jq --arg target_ip "${THING_IP}" --arg target_name "${TARGET_MACHINE}_device" \
   --arg target_port "${TARGET_PORT}" \
      -M 'map(. + {id:"lava", sku:$target_name})
          | (.[0].devices[] | select(.id == "<device-id>") | .connectivity.ip) |= $target_ip
          | (.[0].devices[] | select(.id == "<device-id>") | .connectivity.port) |= ($target_port | tonumber)
          | (.[0].devices[] | select(.id == "<device-id>") | .id) |= $target_name' \
          ${TESTER_CFG_PATH}/device.json | sponge ${TESTER_CFG_PATH}/device.json

cat ${TESTER_CFG_PATH}/device.json
