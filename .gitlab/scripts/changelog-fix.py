# SPDX-FileCopyrightText: Copyright (c) 2024, Linaro Limited.
#
# SPDX-License-Identifier: MIT

"""Fix the changelog for Cassini's non-linear commit history
"""

import re
import sys


def strip_duplicate(line: str, change_ids: []) -> str:
    """Returns the line if it's not a change or it's not already in changes"""

    # Regex for an entry in the changelog, using either markdown or full URLs
    change_re = r'^\- \[(.+)\]\((.*@|https:[a-zA-Z.\/\-]+)[0-9a-f]+\)'

    change = re.match(change_re, line)
    if change:
        change_id = change.group(1)
        if change_id in change_ids:
            print(f'Removing duplicate \'{change_id}\'')
            return None
        change_ids.append(change_id)
    return line


def strip_duplicates(lines: []) -> []:
    """Removes duplicate changelog entries, keeping the oldest (last) entry."""

    change_ids = []
    fixed = [
        new_line for line in reversed(lines) if (
            new_line := strip_duplicate(line, change_ids)
        ) is not None
    ]

    return reversed(fixed)


def strip_empty(lines: []) -> []:
    """Removes empty sections and the following two blank lines"""

    # Regex for an empty section in the changelog
    # Assumes the empty section is followed by two blank lines
    empty_section_re = r'^### (\w+) \(\d+ changes?\)$\n\n\n'

    fixed = re.sub(empty_section_re, '', ''.join(lines), flags=re.MULTILINE)

    return fixed


def main() -> int:
    """Read, fix, and re-write the changelog."""

    # the only argument should be the file being fixed
    file_name = sys.argv[1]

    print(f'Reading {file_name}')
    read = []
    with open(file_name, mode="rt", encoding='utf-8') as file:
        read = file.readlines()

    fixed = strip_empty(strip_duplicates(read))

    print(f'Writing {file_name}')
    with open(file_name, mode="wt", encoding='utf-8') as file:
        file.writelines(fixed)

    return 0


if __name__ == '__main__':
    sys.exit(main())
