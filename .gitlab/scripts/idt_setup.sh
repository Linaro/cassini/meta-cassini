#!/bin/bash
#
# SPDX-FileCopyrightText: Copyright (c) 2024, Linaro Limited.
#
# SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT
#
# This script should be executed only once to setup the project to use
# IoT Device tester with AWS. It creates if not exist the required AWS
# IAM roles, role alias, token exchange role and policies for IoT Device Tester
#
# The AWS CLI should be setup with credentials and configured to run this
# script with the following parameters:
# - AWS_ACCESS_KEY_ID
# - AWS_SECRET_ACCESS_KEY
# - AWS_DEFAULT_REGION
#
# These are the following required parameters for roles and policies:
# - IDT_ROLE
# - IDT_ROLE_POLICY
# - IDT_ROLE_SESSION_DURATION
# - IOT_TE_ROLE
# - IOT_TE_ROLE_POLICY
# - IOT_TE_ROLE_ALIAS
# - IOT_TE_ROLE_POLICY_ALIAS
# - IOT_THING_POLICY
# - IOT_THING_GROUP
# - AWS_BOUNDARY_POLICY (optional)

set -e

# ---------------------------------------------
# Define Fixed Parameters
# ---------------------------------------------
export IDT_ROLE="Proj-Cassini-Greengrass-IDT-Role"
export IDT_ROLE_POLICY="Proj-Cassini-Greengrass-IDT-Policy"
export IOT_TE_ROLE="Proj-Cassini-Greengrass-Token-Exchange-Role"
export IOT_TE_ROLE_POLICY="Proj-Cassini-Greengrass-Token-Exchange-Role-Policy"
export IOT_TE_ROLE_ALIAS="Proj-Cassini-Greengrass-Token-Exchange-Role-Alias"
export IOT_TE_ROLE_POLICY_ALIAS="Proj-Cassini-Greengrass-Token-Exchange-Role-Alias-Policy"
export IOT_THING_POLICY="Device-Proj-Cassini-Greengrass-Thing-Policy"
export IOT_THING_GROUP="Device-Proj-Cassini-Greengrass-Thing-Group"
export IDT_ROLE_SESSION_DURATION="10800"
AWS_BOUNDARY_POLICY=${AWS_BOUNDARY_POLICY:-""}

AWS_ID=$(aws sts get-caller-identity --query "Account" --output text)

if [ -z "$AWS_ID" ]; then
  echo "Can't get AWS Account ID. Check that:"
  echo " - AWS_ACCESS_* env variables have not expired"
  exit 1
else
  export AWS_ID
fi

if [ -n "${AWS_BOUNDARY_POLICY}" ]; then
  PERMISSION_BOUNDARY="--permissions-boundary arn:aws:iam::${AWS_ID}:policy/${AWS_BOUNDARY_POLICY}"
fi

# ----------------------------------------------------------
# Setup the role and policies required for Device Under Test
# ----------------------------------------------------------

# https://docs.aws.amazon.com/greengrass/v2/developerguide/manual-installation.html#create-token-exchange-role
if ! aws iam get-role --role-name ${IOT_TE_ROLE} 2>/dev/null 1>&2; then
  echo "Creating token exchange IAM role ${IOT_TE_ROLE}"

  < .gitlab/scripts/aws_policies/device-role-trust-policy.json envsubst \
                                    >/tmp/device-role-trust-policy.json

  aws iam create-role --role-name ${IOT_TE_ROLE} \
          --assume-role-policy-document file:///tmp/device-role-trust-policy.json \
          ${PERMISSION_BOUNDARY} >/dev/null
  rm -f /tmp/device-role-trust-policy.json
else
  echo "Using existing token exchange IAM role ${IOT_TE_ROLE}"
fi

if ! aws iam get-policy --policy-arn "arn:aws:iam::${AWS_ID}:policy/${IOT_TE_ROLE_POLICY}" 2>/dev/null 1>&2; then
  echo "Creating a role access IAM policy ${IOT_TE_ROLE_POLICY}"

  < .gitlab/scripts/aws_policies/device-role-access-policy.json envsubst \
                                    >/tmp/device-role-access-policy.json

  aws iam create-policy --policy-name ${IOT_TE_ROLE_POLICY} \
                        --policy-document file:///tmp/device-role-access-policy.json >/dev/null

  rm -f /tmp/device-role-access-policy.json
else
  echo "Using existing role access IAM policy ${IOT_TE_ROLE_POLICY}"
fi

echo "Attaching IAM role access policy to IAM token exchange role"
aws iam attach-role-policy --role-name ${IOT_TE_ROLE} \
        --policy-arn "arn:aws:iam::${AWS_ID}:policy/${IOT_TE_ROLE_POLICY}"

# Verify policy attachment for Token Exchange Role
readarray -t VERIFY_IOT_POLICY_ATTACHMENT < <(aws iam list-entities-for-policy --policy-arn "arn:aws:iam::${AWS_ID}:policy/${IOT_TE_ROLE_POLICY}" | jq -r '.PolicyRoles[].RoleName')
verify=false
for each in "${VERIFY_IOT_POLICY_ATTACHMENT[@]}"
do
  if [[ "$IOT_TE_ROLE" == "$each" ]]; then
    verify=true
    break
  fi
done

if [[ $verify == false ]]; then
  echo "IOT_TE - Verification failed for policy attachment"
  exit 1
fi

if ! aws iot describe-role-alias --role-alias ${IOT_TE_ROLE_ALIAS} 2>/dev/null 1>&2; then
  echo "Creating IoT role alias ${IOT_TE_ROLE_ALIAS}"
  aws iot create-role-alias --role-alias ${IOT_TE_ROLE_ALIAS} \
          --role-arn "arn:aws:iam::${AWS_ID}:role/${IOT_TE_ROLE}" >/dev/null
else
  echo "Using existing IoT role alias ${IOT_TE_ROLE_ALIAS}"
fi

role_alias_arn=$(aws iot describe-role-alias --role-alias ${IOT_TE_ROLE_ALIAS} \
                                             --query "roleAliasDescription.roleAliasArn")
export role_alias_arn

if ! aws iot get-policy --policy-name ${IOT_TE_ROLE_POLICY_ALIAS} 2>/dev/null 1>&2; then

  echo "Creating IoT role alias policy ${IOT_TE_ROLE_POLICY_ALIAS}"
  < .gitlab/scripts/aws_policies/greengrass-v2-iot-role-alias-policy.json envsubst \
                                    >/tmp/greengrass-v2-iot-role-alias-policy.json
  aws iot create-policy --policy-name ${IOT_TE_ROLE_POLICY_ALIAS} \
          --policy-document file:///tmp/greengrass-v2-iot-role-alias-policy.json >/dev/null
  rm -f /tmp/greengrass-v2-iot-role-alias-policy.json
else
  echo "Using existing IoT role alias policy ${IOT_TE_ROLE_POLICY}"
fi


# ---------------------------------------------
# Creating Thing Policy
# https://docs.aws.amazon.com/greengrass/v2/developerguide/manual-installation.html#configure-thing-certificate
# ---------------------------------------------
if ! aws iot get-policy --policy-name ${IOT_THING_POLICY} 2>/dev/null 1>&2; then
  echo "Creating IoT thing policy ${IOT_THING_POLICY}"

  < .gitlab/scripts/aws_policies/greengrass-v2-iot-policy.json envsubst \
                                    >/tmp/greengrass-v2-iot-policy.json

  aws iot create-policy --policy-name ${IOT_THING_POLICY} \
          --policy-document file:///tmp/greengrass-v2-iot-policy.json >/dev/null
  rm -f /tmp/greengrass-v2-iot-policy.json
else
  echo "Using existing IoT thing policy ${IOT_THING_POLICY}"
fi

# -------------------------------------------------
# Setup the role and policy required for IDT tester
# -------------------------------------------------

# https://docs.aws.amazon.com/greengrass/v2/developerguide/dev-tst-prereqs.html#config-aws-account-for-idt
if ! aws iam get-role --role-name ${IDT_ROLE} 2>/dev/null 1>&2; then
  echo "Creating IDT IAM role ${IDT_ROLE}"

  < .gitlab/scripts/aws_policies/idt-role-trust-policy.json envsubst \
                                    >/tmp/idt-role-trust-policy.json

  aws iam create-role --role-name ${IDT_ROLE} \
                      --assume-role-policy-document "file:///tmp/idt-role-trust-policy.json" \
                      --max-session-duration "${IDT_ROLE_SESSION_DURATION}" \
                      ${PERMISSION_BOUNDARY} >/dev/null
  rm -f /tmp/idt-role-trust-policy.json
else
  echo "Using existing IDT IAM role ${IDT_ROLE}"
fi

# https://docs.aws.amazon.com/greengrass/v2/developerguide/device-config-setup.html#configure-custom-tes-role-for-idt
if ! aws iam get-policy --policy-arn "arn:aws:iam::${AWS_ID}:policy/${IDT_ROLE_POLICY}" >/dev/null 2>&1; then
  echo "Creating an IDT user role access IAM policy ${IDT_ROLE_POLICY}"

  < .gitlab/scripts/aws_policies/idt-role-access-policy.json envsubst \
                                    >/tmp/idt-role-access-policy.json

  aws iam create-policy --policy-name ${IDT_ROLE_POLICY} \
                        --policy-document file:///tmp/idt-role-access-policy.json >/dev/null
  rm -f /tmp/idt-role-access-policy.json
else
  echo "Using existing IDT user role access IAM policy ${IDT_ROLE_POLICY}"
fi

echo "Attaching IDT user role access IAM policy to IDT IAM role"
aws iam attach-role-policy --role-name ${IDT_ROLE} \
        --policy-arn "arn:aws:iam::${AWS_ID}:policy/${IDT_ROLE_POLICY}"

# Verify policy attachment for the IOT role.
readarray -t VERIFY_IDT_POLICY_ATTACHMENT < <(aws iam list-entities-for-policy --policy-arn "arn:aws:iam::${AWS_ID}:policy/${IDT_ROLE_POLICY}" | jq -r '.PolicyRoles[].RoleName')
for each in "${VERIFY_IDT_POLICY_ATTACHMENT[@]}"
do
  if [[ "$IDT_ROLE" != "$each" ]]; then
    echo "IDT - Verification failed for policy attachment"
    exit 1
  fi
done

echo "Creating Thing Group"
aws iot create-thing-group --thing-group-name "${IOT_THING_GROUP}"

echo "IoT Device Tester (IDT) setup done..."
