..
 # SPDX-FileCopyrightText: Copyright (c) 2023-2025, Linaro Limited.
 #
 # SPDX-FileCopyrightText: <text>Copyright 2022-2024 Arm Limited and/or its
 # affiliates <open-source-office@arm.com></text>
 #
 # SPDX-License-Identifier: MIT

############
Build System
############

A Cassini distribution can be built by setting the target platform via the
``MACHINE`` BitBake variable. In addition, the desired distribution features
via the ``DISTRO_FEATURES`` BitBake variable. Finally, customizing those
features via feature-specific modifiable variables, if needed.

This chapter provides an overview of Cassini's support for the kas build tool.
All the available distribution image features and supported target platforms
are defined together with their associated kas configuration files, followed by
any other additional customization options. The process for building without
kas is then briefly described.

**********************
kas Build Tool Support
**********************

The kas build tool enables automatic fetch and inclusion of layer sources, as
well as parameter and feature specifications for building target images. To
enable this, kas configuration files in the YAML format are passed to the tool
to provide the necessary definitions.

These kas configuration files are modular, where passing multiple files will
result in an image produced with their combined configuration. Further, kas
configuration files can extend other kas configuration files, thereby enabling
specialized configurations that inherit common configurations.

The ``kas`` directory contains kas configuration files that
support building images via kas for the Cassini project, and fall into three
ordered categories:

* **Base Config**
* **Build Modifier Configs**
* **Target Platform Configs**

To build an Cassini distribution image via kas, it is required to provide the
**Base Config** and one **Target Platform Config**, unless otherwise stated
in their descriptions below. Additional **Build Modifier Configs** are
optional, and depend on the target use-case. Currently, it is necessary that
kas configuration files are provided in order: The **Base Config** and then
additional build features via zero or more **Build Modifier Configs**, and
finally the **Target Platform Config**.

To enable builds for a supported target platform or configure each Cassini
distribution image feature, kas configurations files are described in their
relevant sections below: :ref:`target_platforms_label` and
:ref:`distribution_image_features_label`, respectively. Example usage of these
kas configuration files can be found in the :ref:`build_label`
section of the User Manual.

.. note::
  If a kas configuration file does not set a particular build parameter, the
  parameter will take its default value.

.. _target_platforms_label:

****************
Target Platforms
****************

Corstone-1000 for MPS3
======================

  * **Corresponding value for** ``MACHINE`` **variable**: ``corstone1000-mps3``
  * **Target Platform Config**: ``kas/corstone1000-mps3.yml``

  To read documentation about the Corstone-1000, see the
  |Arm Corstone-1000 Technical Overview|_.

  For more information about the software stack for the Corstone-1000, see
  |Arm Corstone-1000 Software|_.

Corstone-1000 FVP
=================

  * **Corresponding value for** ``MACHINE`` **variable**: ``corstone1000-fvp``
  * **Target Platform Config**: ``kas/corstone1000-fvp.yml``

  To read documentation about the Corstone-1000 FVP, see the
  |Fast Models Fixed Virtual Platforms (FVP) Reference Guide|_.

Kria KV260 with U-boot
======================

  * **Corresponding value for** ``MACHINE`` **variable**: ``zynqmp-kria-starter-psa``
  * **Target Platform Config**: ``kas/kv260-psa.yml``

  This supported target platform is Kria KV260,
  implemented in |meta-trustedsubstrate|_.

  This target uses U-Boot as BL33.

  To read the Kria KV260 documentation, see:
  |Kria KV260 User Guide|_ and |Kria KV260 Data Sheet|_

Kria KV260 with EDK-II
======================

  * **Corresponding value for** ``MACHINE`` **variable**: ``zynqmp-kria-starter-psa``
  * **Target Platform Config**: ``kas/kv260-edk2.yml``

  This supported target platform is Kria KV260,
  implemented in |meta-trustedsubstrate|_.

  This target uses EDK-II as BL33.

Generic Arm64
=============

  * **Corresponding value for** ``MACHINE`` **variable**: ``genericarm64``
  * **Target Platform Config**: ``kas/genericarm64.yml``

  This MACHINE is used to represent a generic 64-bit platform that implements
  the Arm SystemReady specification, has a working firmware and can boot via EFI.

  This machine configuration does not build any firmware components.

All the above include common configuration from ``kas/include/arm-base.yml`` which
defines layers and dependencies required when building for all Arm-based
platforms.

Moreover, ``corstone1000-fvp.yml``, and ``corstone1000-mps3.yml``
**Target Platform Config** include common configuration for Arm-maintained platforms
from ``kas/include/arm-machines.yml`` which defines the
BSPs, layers, and dependencies required when building for each platform.

While ``kv260-psa.yml`` and ``kv260-edk2.yml`` **Target Platform Config** include configuration
from ``kas/include/xilinx-machines.yml`` which defines specific layers and dependencies
required when building for this platform.

.. _distribution_image_features_label:

***************************
Distribution Image Features
***************************

For a particular target platform, the available Cassini distribution image
features (corresponding to the contents of the ``DISTRO_FEATURES`` BitBake
variable) are detailed in this section, along with any associated kas
configuration files, and any associated customization options relevant for that
feature.

Cassini Architecture
====================

Cassini distribution image can be configured via kas using **Base Config**.
This includes a set of common configuration from a base Cassini kas
configuration file:

  * ``kas/include/cassini-base.yml``

  This kas configuration file defines the base Cassini layer dependencies and
  their software sources, as well as additional build configuration variables.
  It also includes the ``kas/include/cassini-release.yml``
  kas configuration file, where the layers dependencies are pinned for any
  corresponding Cassini release.

  * **Corresponding value in** ``DISTRO`` **variable**:
    ``cassini``.
  * **Base Config**: ``kas/cassini.yml``.

  This Cassini distribution image feature enables the ``cassini-image-base``
  build target, to build an Cassini distribution image.

  The **Base Config** for this distribution image feature sets the build
  target to ``cassini-image-base``.

  To build Cassini distribution image, provide the **Base Config** to the kas
  build command. For example, to build a Cassini distribution image for the
  KV260 hardware target platform, run the following command:

    .. code-block:: console

      kas build kas/cassini.yml:kas/kv260-psa.yml

Other Cassini Features
======================

Developer Support
-----------------

  * **Corresponding value in** ``DISTRO_FEATURES`` **variable**:
    ``cassini-dev``.
  * **Build Modifier Config**: ``kas/dev.yml``.

  This Cassini distribution feature includes packages appropriate for
  development, such as the ``debug-tweaks`` package (which sets
  an empty root password) and removing security-hardening features.

    .. code-block:: console

      kas build kas/cassini.yml:kas/dev.yml:kas/kv260-psa.yml

.. _build_system_run-time_integration_tests_label:

Run-Time Integration Tests
--------------------------

  * **Corresponding value in** ``DISTRO_FEATURES`` **variable**:
    ``cassini-test``.
  * **Build Modifier Config**: ``kas/tests.yml``.

    This Cassini distribution feature includes the Cassini test suites
    provided to validate the image is running successfully with the expected
    Cassini functionalities.

    The Build Modifier for this distribution image feature automatically
    includes the Yocto Package Test (ptest) framework in the image, configures
    the inclusion of ``meta-cassini-tests`` as a Yocto layer source for the
    build, and appends the ``cassini-test`` feature to ``DISTRO_FEATURES`` for
    the build.

    To include run-time integration tests in a Cassini distribution image,
    provide the **Build Modifier Config** to the kas build command. For
    example, to include the tests in a Cassini distribution image for the KV260
    hardware target platform, run the following command:

    .. code-block:: console

      kas build kas/cassini.yml:kas/tests.yml:kas/kv260-psa.yml

    The size of the root filesystem is extended via the
    ``CASSINI_ROOTFS_EXTRA_SPACE`` BitBake variable, to ``2000000``
    Kilobytes, which is required by this integration tests if
    :ref:`parsec_service_label` is enabled.

    Each suite of run-time integration tests and specific customizable
    variables associated with each suite are detailed separately, at
    :ref:`run-time_integration_tests_label`.

Cloud Service
-------------

This Cassini distribution feature includes container runtime and orchestration 
components.

``VIRTUAL-RUNTIME_cloud_service`` is used to select one of three options.


No Cloud
^^^^^^^^

  * **Corresponding value in** ``DISTRO_FEATURES`` **variable**:
    ``cassini-cloud``.
  * **Corresponding value in** ``VIRTUAL-RUNTIME_cloud_service`` **variable**:
    ``no-cloud``.
  * **Build Cloud Configs**: ``kas/no-cloud.yml``.

    This Cassini distribution feature provides an image with
    a default container runtime option (podman) from meta-virtualization
    without including any cloud provider.

    To build a Cassini distribution image without a cloud provider,
    provide the **Build Cloud Config** to the kas build command. For
    example:


    .. code-block:: console

      kas build kas/cassini.yml:kas/no-cloud.yml:kas/kv260-psa.yml

.. _k3s_orchestration_label:

K3s orchestration
^^^^^^^^^^^^^^^^^

  * **Corresponding value in** ``DISTRO_FEATURES`` **variable**:
    ``cassini-cloud``.
  * **Corresponding value in** ``VIRTUAL-RUNTIME_cloud-service` **variable**:
    ``k3s-cloud``.
  * **Build Cloud Configs**: N/A (enabled by default).

    This Cassini distribution feature includes the K3s cloud orchestration.

    If :ref:`build_system_run-time_integration_tests_label` Build Modifier
    for this distribution image feature then it will automatically
    include the required :ref:`k3s_orchestration_tests_label` test in the ptest
    framework of the image.

    To include K3s orchestration in a Cassini distribution image,
    provide the **Build Cloud Config** to the kas build command. For
    example:

    The size of the root filesystem is extended via the
    ``CASSINI_ROOTFS_EXTRA_SPACE`` BitBake variable, to ``2000000``
    Kilobytes, which is required by this cloud orchestration.

    .. code-block:: console

      kas build kas/cassini.yml:kas/kv260-psa.yml


AWS IoT Greengrass
^^^^^^^^^^^^^^^^^^

  * **Corresponding value in** ``DISTRO_FEATURES`` **variable**:
    ``cassini-cloud``.
  * **Corresponding value in** ``VIRTUAL-RUNTIME_cloud_service`` **variable**:
    ``greengrass-cloud``.
  * **Build Cloud Configs**: ``kas/greengrass.yml``.

    This Cassini distribution feature includes the AWS IoT Greengrass cloud service.

    To include AWS IoT Greengrass cloud service in a Cassini distribution image,
    provide the **Build Cloud Config** to the kas build command. For
    example:

    The size of the root filesystem is extended via the
    ``CASSINI_ROOTFS_EXTRA_SPACE`` BitBake variable, to ``2000000``
    Kilobytes, which is required by this cloud service.

    .. code-block:: console

      kas build kas/cassini.yml:kas/greengrass.yml:kas/kv260-psa.yml

Over-the-Air Update
-------------------

This Cassini distribution feature includes over-the-air update support
using mender.

``VIRTUAL-RUNTIME_ota_update`` is used to select one of two options.

Mender
^^^^^^

  * **Corresponding value in** ``DISTRO_FEATURES`` **variable**:
    ``cassini-ota``.
  * **Corresponding value in** ``VIRTUAL-RUNTIME_ota_update`` **variable**:
    ``mender-ota``.

    This Cassini distribution feature includes the mender client and relevant
    services by default.

No OTA
^^^^^^

  * **Corresponding value in** ``DISTRO_FEATURES`` **variable**:
    ``cassini-ota``.
  * **Corresponding value in** ``VIRTUAL-RUNTIME_ota_update`` **variable**:
    ``no-ota``.
  * **Build Cloud Configs**: ``kas/no-ota.yml``.

    This Cassini distribution feature provides an image without
    any Over-the-air update client.

    To remove the update client in Cassini distribution image, provide
    the **Build OTA Update Config** to the kas build command. For
    example:

    .. code-block:: console

      kas build kas/cassini.yml:kas/no-ota.yml:kas/kv260-psa.yml



Security Service
----------------

This Cassini distribution feature configures parsec-service backend for
the Cassini distribution image.

``VIRTUAL-RUNTIME_security_provider`` is used to select one of two options.

PSA Provider
^^^^^^^^^^^^

  * **Corresponding value in** ``VIRTUAL-RUNTIME_security_provider`` **variable**:
    ``psa-provider``.
  * **Build Cloud Configs**: ``kas/psa-security.yml``.

    This Cassini distribution feature provides an image with
    parsec services that uses Trusted-Services as the backend for
    crypto operations.

    To build a Cassini distribution image with Trusted-Services as the parsec
    backend, provide the **Build Security Provider Config** to the kas
    build command.
    For example:


    .. code-block:: console

      kas build kas/cassini.yml:kas/psa-security.yml:kas/kv260-psa.yml

SW Provider
^^^^^^^^^^^

  * **Corresponding value in** ``VIRTUAL-RUNTIME_security_provider`` **variable**:
    ``sw-provider``.
  * **Build Cloud Configs**: ``kas/sw-security.yml``.

    This Cassini distribution feature provides an image with
    parsec services that uses Mbed Crypto as the backend for
    crypto operations.

    To build a Cassini distribution image with Mbed Crypto as the parsec
    backend, provide the **Build Security Provider Config** to the kas
    build command.
    For example:


    .. code-block:: console

      kas build kas/cassini.yml:kas/sw-security.yml:kas/kv260-psa.yml


.. _parsec_service_label:

Parsec service
--------------

    **Corresponding value in** ``DISTRO_FEATURES`` **variable**:
    ``cassini-parsec``.

    This Cassini distribution feature adds parsec-service and parsec-tool
    to the Cassini distribution image.

    The value ``cassini-parsec`` is appended to ``DISTRO_FEATURES`` in
    ``meta-cassini-distro/conf/distro/cassini.conf``. Therefore, parsec service
    is included in the Cassini distribution image by default. If parsec-service
    is not required then the value ``cassini-parsec`` can be removed from
    ``DISTRO_FEATURES`` in the ``<distro name>.conf`` of the downstream
    distribution. To build Cassini distribution image with parsec-service for
    the KV260 hardware target platform, run the following command:

    .. code-block:: console

      kas build kas/cassini.yml:kas/kv260-psa.yml

.. _security_hardening_label:

Security Hardening
------------------

  * **Corresponding value in** ``DISTRO_FEATURES`` **variable**:
    ``cassini-security``.

    This Cassini distribution feature configures user accounts, packages,
    remote access controls and other image features to provide extra security
    hardening for the Cassini distribution image.

    Security hardening is enabled by default in the Cassini distribution image
    and the base configuration appends the ``cassini-security`` feature to
    ``DISTRO_FEATURES`` for the build. To remove it in the Cassini distribution
    image, ``kas/dev.yml`` can be used.
    For example:

    .. code-block:: console

      kas build kas/cassini.yml:kas/dev.yml:kas/kv260-psa.yml

    The security hardening is described in more detail at
    :ref:`Security Hardening<developer_manual/security_hardening:Security Hardening>`.

.. _software_development_kit_label:

********************************************
Additional Distribution Image Customizations
********************************************

An additional set of customization options are available for Cassini
distribution images, which don't fall under a distinct distribution image
feature. These customizations are listed below and are grouped by the
customization target.

Filesystem Customization
========================

Adding Extra Rootfs Space
-------------------------

The size of the root filesystem can be extended via the
``CASSINI_ROOTFS_EXTRA_SPACE`` BitBake variable. The value of this variable
is appended to the ``IMAGE_ROOTFS_EXTRA_SPACE`` BitBake variable.

Tuning the Filesystem Compilation
---------------------------------

The Cassini filesystem by default uses the generic ``armv8a-crc`` tune for
``aarch64`` based target platforms. This reduces build times by increasing the
sstate-cache reused between different image types and target platforms. This
optimization can be disabled by setting ``CASSINI_GENERIC_ARM64_FILESYSTEM`` to
``"0"``. The file system compilation tune used when
``CASSINI_GENERIC_ARM64_FILESYSTEM`` is enabled can be changed by setting
``CASSINI_GENERIC_ARM64_DEFAULTTUNE``, which configures the ``DEFAULTTUNE``
BitBake variable for the ``aarch64`` based target platforms builds. See
|DEFAULTTUNE|_ for more information.

In summary, the relevant variables and their default values are:

  .. code-block:: yaml

    CASSINI_GENERIC_ARM64_FILESYSTEM: "1"             # Enable generic file system (1 or 0).
    CASSINI_GENERIC_ARM64_DEFAULTTUNE: "armv8a-crc"   # Value of DEFAULTTUNE if generic file system enabled.

Their values can be set by passing them as environmental variables. For
example, the optimization can be disabled using:

  .. code-block:: console

        CASSINI_GENERIC_ARM64_FILESYSTEM="0" kas build kas/cassini.yml:kas/kv260-psa.yml

**************************
Manual BitBake Build Setup
**************************

In order to build an Cassini distribution image without the kas build tool
directly via BitBake, it is necessary to prepare a BitBake project as follows:

  * Configure :ref:`dependent Yocto layers <layer_dependency_overview_label>`
    in ``bblayers.conf``.
  * Configure the ``DISTRO`` as ``cassini`` in ``local.conf``.
  * Configure the image ``DISTRO_FEATURES`` in ``local.conf``.

Assuming correct environment configuration, the BitBake build can then be run
for the desired image target corresponding to one of the following:

  * ``cassini-image-base``

As the kas build configuration files within the ``kas/``
directory define the recommended build settings for each feature. Any
additional functionalities may therefore be enabled by reading these
configuration files and manually inserting their changes into the BitBake build
environment.
