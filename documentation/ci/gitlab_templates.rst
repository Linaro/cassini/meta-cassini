..
 # SPDX-FileCopyrightText: Copyright (c) 2023, Linaro Limited.
 #
 # SPDX-License-Identifier: MIT

################
GitLab Templates
################

The |GitLab Templates|_ project defines common CI/CD configuration elements
which can be included in other GitLab projects. The components used in
the Cassini CI/CD pipelines are:

* **Changelog**:

  This updates the CHANGELOG.md file when a merge request modifies
  CASSINI_VERSION in cassini-release.yml. This will cause a new
  commit added to the MR and cancel/re-trigger the pipeline.

* **Child pipelines**:

  This creates the merge, trigger, and collate-results jobs for the
  child pipeline.

* **Workflow**:

  This defines some common rules to control when CI pipelines should
  and shouldn't run. For example, don't run the pipeline for pushes to
  a branch when there is already an MR open for that branch

* **Danger review**:

  This job runs the danger-bot on merge request pipelines to report
  issues early.

* **Docker images**:

  Set up Docker images with a predefined set of configurations to be
  used by the pipeline using `buildah`.

* **Static analyzer**:

  Sets up Sast (Security analyzer), and Code quality
  (Code quality analyzer) and generates Code quality HTML reports
  using code climate plugins

* **Lava testing**:

  Setup to submit jobs to LAVA Test framework and retrieve results when
  they are complete.

* **Auto release**:

  These jobs create a GitLab release and attach release notes based on
  changes to the Changelog. The release note is generated from the git
  commit history and requires knowledge of the current package version number.

* **Kas setup**:

  Set up the required configurations for building a Yocto-based project
  using `kas`.
