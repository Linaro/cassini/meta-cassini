..
 # SPDX-FileCopyrightText: Copyright (c) 2023-2024, Linaro Limited.
 #
 # SPDX-License-Identifier: MIT

###############
GitLab pipeline
###############

A brief overview of GitLab pipeline

***************
Parent pipeline
***************
.. _parent_pipeline_label:

This pipeline is responsible for setup, configuring tools in the Docker
images, and performing static analysis (Code Quality, Danger-Review,
secret detection, and container scanning).

|

.. image:: ../images/gitlab_parent_pipeline.svg
  :align: center

|

* **.pre**:

  * Build the Docker images required for the project
    (For example debian-buster OS) for different architectures and push them
    to the registry. This involves IDT Greengrass, LAVA and Utility Docker
    (arm64, x86_64) images.
  * Pull the generated Docker images and config and create a manifest for
    LAVA and Utility usage.
  * Run danger-bot for reviewing and report issues early.
  * Regenerate the changelog when the project version number is changed.

* **Setup**:

  * Setup stage to ensure all the configurations have a valid yaml file.
  * Merge all the jobs into one file which defines each stage of the
    child pipeline.

* **Build**:

  * Build the documentation.
  * Create a child pipeline.

* **Test**:

  * Collate-results from :ref:`child pipeline <child_pipeline_label>`.
  * Detect any secrets present in the codebase.
  * Test the code quality using Code climate plugin.
  * Generate code quality report using Code climate plugin.
  * Run Gitlab semgrep analyzer.

* **Release**:

  * Creating the release and notes.


**************
Child pipeline
**************
.. _child_pipeline_label:

This pipeline is responsible to build cassini distro images, setup, submit
and report back results from the LAVA test framework perform.

|

.. image:: ../images/gitlab_child_pipeline.svg
  :align: center

|

* **Setup**:

  * Update external repositories that are required for Cassini distribution.
  * Extract the required FVP version details from the codebase.

* **Build**:

  * Image build for all supported platforms depending on rules or changes
    to the codebase.
  * Check if Cassini distro is compatible with layers definitions, this
    is based on each platform and multiple layers included in the distro.

* **Test**:

  * Install the required FVP in a Docker image.
  * Prepare the built Cassini distro images for LAVA test framework.
  * Submit jobs to the LAVA test framework.
  * Wait for event from LAVA test framework for completion and return
    to :ref:`parent pipeline <parent_pipeline_label>`

* **Cleanup**:

  * Clean the sstate cache and download directory which is older than
    specific number of days.
