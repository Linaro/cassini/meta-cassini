..
 # SPDX-FileCopyrightText: Copyright (c) 2023, Linaro Limited.
 #
 # SPDX-FileCopyrightText: <text>Copyright 2022-2023 Arm Limited and/or its
 # affiliates <open-source-office@arm.com></text>
 #
 # SPDX-License-Identifier: MIT

############
Contributing
############

We welcome contribution from everyone via the ``meta-cassini`` public Gitlab
repository: |meta-cassini repository|. For general introduction about Cassini
distribution, refer to :doc:`introduction`.

*******
License
*******

The Cassini distribution is released under :ref:`license_link:License`.

Please use an `SPDX license identifier <https://spdx.org/licenses/>`_ in every source file following the
`recommendations <https://spdx.github.io/spdx-spec/v2.2.2/using-SPDX-short-identifiers-in-source-files/#e2-format-for-spdx-license-identifier>`_ to make it easier
for users to understand and review licenses.

::

    /*
    * SPDX-License-Identifier: MIT
    */

.. _contributing_to_cassini_distribution_label:

************************************
Contributing to Cassini distribution
************************************

This project uses the GitLab `project forking workflow <https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html>`_.

Every commit must have at least one ``Signed-off-by:`` line from the committer
to certify that the contribution is made under the terms of the
``Developer's Certificate of Origin``.

The full text of ``Developer's Certificate of Origin`` can be found
in `sign-your-work-the-developer-s-certificate-of-origin <https://www.kernel.org/doc/html/latest/process/submitting-patches.html#sign-your-work-the-developer-s-certificate-of-origin>`_.
Due to the significance of the ``Developer's Certificate of Origin``, part of
it is copied below.

::

    The sign-off is a simple line at the end of the explanation for the
    patch, which certifies that you wrote it or otherwise have the right to
    pass it on as an open-source patch.  The rules are pretty simple: if you
    can certify the below:

    Developer's Certificate of Origin 1.1
    ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    By making a contribution to this project, I certify that:

        (a) The contribution was created in whole or in part by me and I
            have the right to submit it under the open source license
            indicated in the file; or

        (b) The contribution is based upon previous work that, to the best
            of my knowledge, is covered under an appropriate open source
            license and I have the right under that license to submit that
            work with modifications, whether created in whole or in part
            by me, under the same open source license (unless I am
            permitted to submit under a different license), as indicated
            in the file; or

        (c) The contribution was provided directly to me by some other
            person who certified (a), (b) or (c) and I have not modified
            it.

        (d) I understand and agree that this project and the contribution
            are public and that a record of the contribution (including all
            personal information I submit with it, including my sign-off) is
            maintained indefinitely and may be redistributed consistent with
            this project or the open source license(s) involved.

    then you just add a line saying::

        Signed-off-by: Random J Developer <random@developer.example.org>

    using your real name (sorry, no pseudonyms or anonymous contributions.)

.. _commit_guidelines_label:

*****************
Commit guidelines
*****************

Commits and patches added should follow the `OpenEmbedded patch guidelines <https://www.openembedded.org/wiki/Commit_Patch_Message_Guidelines>`_ with the following additions.

The component being changed in the shortlog should be prefixed with the
layer name (without meta-), for example:

    cassini-config: Decrease frobbing level

    cassini-distro: Enable foobar v2

    cassini-doc: Added foobar v2 documentation

While specific to the Linux kernel, refer also to the `Linux kernel patch guidance <https://docs.kernel.org/process/submitting-patches.html>`_.
In the above, pay particular attention to the guidance on how to make your changes easy to review.

==========================
Describe your changes
==========================

Describe the problem you are fixing or the feature you are adding.
The commits themselves show **how** the code is being changed so the commit messages should explain to the reviewer (in plain English) **what** is being changed and **why**.

==========================
Separate your changes
==========================

Separate each logical change into a separate commit.
Each commit should implement a single, cohesive idea which should be justifiable on its own merits.
Separate complex commits by dividing large problems or features into smaller ideas which can be applied one at a time.
A commit which makes similar changes to multiple files should be separated from a commit which makes an unrelated change to a single file.

==========================
Commit messages guidelines
==========================

Commit messages should follow the guidelines below, for reasons explained by Chris Beams in `How to Write a Git Commit Message <https://cbea.ms/git-commit/>`_:

- The commit subject and body must be separated by a blank line.
- The commit subject must start with a capital letter.
- The commit subject must not be longer than 72 characters.
- The commit subject must not end with a period.
- The commit body must not contain more than 72 characters per line.
- Commits that change 30 or more lines across at least 3 files should
  describe these changes in the commit body.
- Use issues and merge requests' full URLs instead of short references,
  as they are displayed as plain text outside of GitLab.
- The merge request should not contain more than 10 commit messages.
- The commit subject should contain at least 3 words.

**Important notes:**

- If the guidelines are not met, the MR may not pass the `Danger checks <https://gitlab.com/Linaro/cassini/meta-cassini/-/blob/main/Dangerfile>`_.
- Consider enabling `Squash and merge <https://docs.gitlab.com/ee/user/project/merge_requests/squash_and_merge.html>`_
  if your merge request includes "Applied suggestion to X files" commits, so that Danger can ignore those.
- The prefixes in the form of *[prefix]* and *prefix:* are allowed (they can be all lowercase, as long
  as the message itself is capitalized). For instance, *danger: Improve Danger behavior* and
  *[API] Improve the labels endpoint* are valid commit messages.

--------------------------
Why these standards matter
--------------------------

1. Consistent commit messages that follow these guidelines make the history more readable.
#. Concise standard commit messages helps to identify breaking changes for a deployment or ~"main:broken" quicker when
   reviewing commits between two points in time.

-----------------------
Commit message template
-----------------------

Example commit message template that can be used on your machine that embodies the above (guide for `how to apply template <https://codeinthehole.com/tips/a-useful-template-for-commit-messages/>`_)::

    # (If applied, this commit will...) <subject>        (Max 72 characters)
    # |<----          Using a Maximum Of 72 Characters                ---->|


    # Explain why this change is being made
    # |<----   Try To Limit Each Line to a Maximum Of 72 Characters   ---->|

    # Provide links or keys to any relevant tickets, articles or other resources
    # Use issues and merge requests' full URLs instead of short references,
    # as they are displayed as plain text outside of GitLab

    # --- COMMIT END ---
    # --------------------
    # Remember to
    #    Capitalize the subject line
    #    Use the imperative mood in the subject line
    #    Do not end the subject line with a period
    #    Subject must contain at least 3 words
    #    Separate subject from body with a blank line
    #    Commits that change 30 or more lines across at least 3 files should
    #    describe these changes in the commit body
    #    Use the body to explain what and why vs. how
    #    Can use multiple lines with "-" for bullet points in body
    #    For more information: https://cbea.ms/git-commit/
    # --------------------


*****************
Changelog entries
*****************
This section contains instructions for when and how to generate a changelog entry
file, as well as information and history about our changelog process.

========
Overview
========
Each bullet point, or **entry**, in our `CHANGELOG.md <https://gitlab.com/Linaro/cassini/meta-cassini/-/blob/main/CHANGELOG.md>`_ file is generated from the subject line of a Git commit. Commits are included
when they contain the Changelog `Git trailer <https://git-scm.com/docs/git-interpret-trailers>`_.  When generating the changelog, author and merge request details are added
automatically.

The Changelog trailer accepts the following values:

- feature: New feature added/enabled
- bug: Bug fix
- deprecated: New deprecation
- removed: Feature removal
- security: Security fix
- performance: Performance improvement
- other: Other

An example of a Git commit to include in the changelog is the following::

    Update git vendor to gitlab

    Now that we are using gitaly to compile git, the git version isn't known
    from the manifest, instead we are getting the gitaly version. Update our
    vendor field to be `gitlab` to avoid cve matching old versions.

    Changelog: changed
    MR: https://gitlab.com/foo/bar/-/merge_requests/123


---------------------------------------
Overriding the associated merge request
---------------------------------------

GitLab automatically links the merge request to the commit when generating the changelog. If you want to override the merge request to link to, you can specify
an alternative merge request using the MR trailer::

    Update git vendor to gitlab

    Now that we are using gitaly to compile git, the git version isn't known
    from the manifest, instead we are getting the gitaly version. Update our
    vendor field to be `gitlab` to avoid cve matching old versions.

    Changelog: changed
    MR: https://gitlab.com/foo/bar/-/merge_requests/123

The value must be the full URL of the merge request.

================================
What warrants a changelog entry?
================================

- Security fixes **must** have a changelog entry, with ``Changelog`` trailer set to ``security``.
- Any user-facing change **must** have a changelog entry. Example: "meta-cassini now supports AWS Greengrass as a cloud option"
- A fix for a regression introduced and then fixed in the same release (such as fixing a bug introduced during a release candidate) **should not** have a changelog entry.
- Any developer-facing change (such as refactoring, technical debt remediation, or test suite changes) **should not** have a changelog entry.
- Any contribution from a community member, no matter how small, **may** have a changelog entry regardless of these guidelines if the contributor wants one.
- Any experimental changes **should not** have a changelog entry.
- An MR that includes only documentation changes **should not** have a changelog entry.

==============================
Writing good changelog entries
==============================

A good changelog entry should be descriptive and concise. It should explain the change to a reader who has *zero context* about the change. If you have trouble making it both concise and descriptive, err on the side of descriptive.

- **Bad:** Use newest version.
- **Good:** Updated to latest U-Boot version to get FF-A support.

The first example provides no context of where the change was made, or why, or how it benefits the user.

- **Bad:** Update syntax.
- **Good:** Update bitbake files to new append syntax to allow use with > hardknott yocto versions.

Again, the first example is too vague and provides no context.

- **Bad:** Fixes and Improves , usage in config files.
- **Good:** Fix parsec config file so that parsec can encrypt large payloads from clients.

The first example is too focused on implementation details. The user doesn't care that we changed comma's they care about the *end result* of those changes.

- **Bad:** Extended parsec input buffer for encrypt operations
- **Good:** Allow parsec to encrypt message up to 512Kb in size when using incremental encryption API's

The first example focuses on *how* we fixed something, not on *what* it fixes.  The rewritten version clearly describes the *end benefit* to the user (larger possible data sets), and *when* (calling the incremental encryption API's).

Use your best judgement and try to put yourself in the mindset of someone reading the compiled changelog. Does this entry add value? Does it offer context about *where* and *why* the change was made?

=================================
How to generate a changelog entry
=================================

Git trailers are added when committing your changes. This can be done using your text editor of choice. Adding the trailer to an existing commit requires either amending to the commit (if it's the most recent one), or an interactive rebase using ``git rebase -i``.

To update the last commit, run the following::

    git commit --amend

You can then add the Changelog trailer to the commit message. If you had already pushed prior commits to your remote branch, you have to force push the new commit::

    git push -f origin your-branch-name

To edit older (or multiple commits), use ``git rebase -i HEAD~N`` where ``N`` is the last N number of commits to rebase. Let's say you have 3 commits on your branch: A, B, and C. If you want to update commit B, you need to run::

    git rebase -i HEAD~2

This starts an interactive rebase session for the last two commits. When started, Git presents you with a text editor with contents along the lines of the following::

    pick B Subject of commit B
    pick C Subject of commit C

To update commit B, change the word pick to reword, then save and quit the editor. Once closed, Git presents you with a new text editor instance to edit the commit message of commit B. Add the trailer, then save  and quit the editor.  If all went well, commit B is now updated.

For more information about interactive rebases, take a look at the `Git documentation <https://git-scm.com/book/en/v2/Git-Tools-Rewriting-History>`_.

******************
Submitting changes
******************

Thank you for your interest in contributing to Cassini distribution.
To contribute, follow the :ref:`instructions <contributing_to_cassini_distribution_label>`
and ensure you adhere to :ref:`commit guidelines <commit_guidelines_label>`.

**************
Merge criteria
**************

* The merge request must receive at least 2 approvals from
  :ref:`Cassini distro maintainers <maintainers_label>`
* ``meta-cassini`` pipelines are passed
* No regression on code coverage
