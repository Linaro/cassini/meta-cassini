..
 # SPDX-FileCopyrightText: Copyright (c) 2024, Linaro Limited.
 #
 # SPDX-License-Identifier: MIT

#########################################
Getting Started with Generic Arm64 Images
#########################################

This document explains how to build and deploy a generic Arm64 distro image
to an SD card.

.. note::
  Refer to the platform's user manual for details on how to configure the
  platform to boot the deployed image.

******************************
Building Generic Arm64 Images
******************************

The provided kas configuration file ``kas/genericarm64.yml``
can be used to build images which target the arm64 machines.
To build an image with default options:

.. code-block:: console

  kas build --update kas/cassini.yml:kas/genericarm64.yml

This will produce a Cassini generic Arm64 distribution image here:

  ``build/tmp/deploy/images/genericarm64/cassini-image-base-genericarm64.rootfs.wic.gz``

  ``build/tmp/deploy/images/genericarm64/cassini-image-base-genericarm64.rootfs.wic.bmap``

For other build options, refer to :doc:`../developer_manual/build_system`

.. note::

  This machine does not build any firmware components.
  For firmware build options, please refer to the platform specific
  guidelines.

**************************
Flashing the Distro Image
**************************

1. Insert the SD card into **the host machine**

2. Check if the SD card is seen by **the host machine** via ``lsblk``.

  This will output, for example:

  .. code-block:: shell

    NAME        MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
    <sd card>   179:0    0    16G  0 disk
    ├─p1        179:1    0   256M  0 part
    └─p2        179:2    0   512M  0 part

3. Flash the image onto the SD card using ``bmap-tools``:

  .. code-block:: shell

    sudo bmaptool copy --bmap cassini-image-base-genericarm64.rootfs.wic.bmap cassini-image-base-genericarm64.rootfs.wic.gz /dev/<sd card>

4. Eject the SD card from **the host machine**, and insert it into any arm64 machine
