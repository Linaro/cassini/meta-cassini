# SPDX-FileCopyrightText: Copyright (c) 2024, Linaro Limited.
#
# SPDX-FileCopyrightText: <text>Copyright 2022-2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-FileCopyrightText: Copyright (c) 2024, Linaro Limited.
#
# SPDX-License-Identifier: MIT

SUMMARY = "Cassini test account creation"
DESCRIPTION = "Creates an 'CASSINI_TEST_ACCOUNT' account used for running CASSINI \
               runtime integration tests and installs common include files."
HOMEPAGE = "https://cassini.readthedocs.io/en/latest/"

LICENSE = "MIT"
# License file is in "layers/poky/meta/files/common-licenses".
# nooelint: oelint.var.licenseremotefile
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

# Default values, used only if 'meta-cassini-distro/conf/distro/cassini.conf'
# is skipped.
CASSINI_TEST_ACCOUNT ??= "test"
CASSINI_ADMIN_GROUP ??= "sudo"

# Comma separated list of groups for 'CASSINI_TEST_ACCOUNT' user
CASSINI_TEST_GROUPS = "${CASSINI_ADMIN_GROUP}"

DEPENDS:append = " gettext-native"

SRC_URI = "file://integration-tests-common-funcs.sh \
           file://container-engine-funcs.sh \
           file://login-console-funcs.expect \
           file://run-command.expect"

inherit allarch useradd
require runtime-integration-tests.inc

USERADD_PACKAGES = "${PN}"
USERADD_PARAM:${PN} = "--create-home \
                       --password '' \
                       --groups ${CASSINI_TEST_GROUPS} \
                       --user-group ${CASSINI_TEST_ACCOUNT};"

do_configure[noexec] = "1"
do_compile[noexec] = "1"

do_install() {
    install -d "${D}/${TEST_COMMON_DIR}"

    envsubst '$TEST_RUNTIME_DIR' < "${UNPACKDIR}/integration-tests-common-funcs.sh" \
        > "${D}/${TEST_COMMON_DIR}/integration-tests-common-funcs.sh"

    envsubst '$TEST_RUNTIME_DIR' < "${UNPACKDIR}/container-engine-funcs.sh" \
        > "${D}/${TEST_COMMON_DIR}/container-engine-funcs.sh"

    install --mode="644" "${UNPACKDIR}/login-console-funcs.expect" \
        "${D}/${TEST_COMMON_DIR}"

    envsubst '$TEST_COMMON_DIR' < "${UNPACKDIR}/run-command.expect" \
        > "${D}/${TEST_COMMON_DIR}/run-command.expect"
}

FILES:${PN} += "${TEST_COMMON_DIR}"

# nooelint: oelint.vars.specific - Distro feature name
RDEPENDS:${PN}:append:cassini-security = " expect"
