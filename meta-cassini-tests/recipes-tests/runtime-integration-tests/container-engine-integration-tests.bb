# SPDX-FileCopyrightText: Copyright (c) 2024, Linaro Limited.
#
# SPDX-FileCopyrightText: <text>Copyright 2022-2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

SUMMARY = "Container engine integration tests."
DESCRIPTION = "Integration tests for the Docker container engine. \
               Tests may be run standalone via \
               run-container-engine-integration-tests, or via the ptest \
               framework using ptest-runner."
HOMEPAGE = "https://cassini.readthedocs.io/en/latest/"

LICENSE = "MIT"
# License file is in "layers/poky/meta/files/common-licenses".
# nooelint: oelint.var.licenseremotefile
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

TEST_FILES = "file://container-engine-integration-tests.bats"

inherit runtime-integration-tests
require runtime-integration-tests.inc
