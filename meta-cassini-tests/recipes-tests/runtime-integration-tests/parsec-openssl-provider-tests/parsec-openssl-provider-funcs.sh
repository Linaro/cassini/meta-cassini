#!/usr/bin/env bash
#
# SPDX-FileCopyrightText: Copyright (c) 2024, Linaro Limited.
#
# SPDX-License-Identifier: MIT

# Arg1: Container name or ID
# Arg2: Script path
container_execute_bash_script() {

    container_id="${1}"
    script_path="${2}"

    sudo -n docker exec -i "${container_id}" /bin/bash < "${script_path}" \
        2>"${TEST_STDERR_FILE}"
}
