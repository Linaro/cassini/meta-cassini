#!/usr/bin/env bats
#
# SPDX-FileCopyrightText: Copyright (c) 2024, Linaro Limited.
#
# SPDX-License-Identifier: MIT

# Run-time validation tests for the Parsec Openssl Provider on a CASSINI system.

# Set generic configuration

if [ -z "${POP_TEST_LOG_DIR}" ]; then
    TEST_LOG_DIR="${HOME}/runtime-integration-tests-logs"
else
    TEST_LOG_DIR="${POP_TEST_LOG_DIR}"
fi

export TEST_LOG_FILE="${TEST_LOG_DIR}/parsec-openssl-provider-tests.log"
export TEST_STDERR_FILE="${TEST_LOG_DIR}/pop-stderr.log"
export TEST_RUN_FILE="${TEST_RUNTIME_DIR}/parsec-openssl-provider-tests.pgid"

# Set test-suite specific configuration

POP_TEST_IMAGE="registry.gitlab.com/linaro/cassini/meta-cassini/parsec-openssl-provider-image:latest"
POP_TEST_CONTAINER_NAME="Parsec-OpenSSL-provider"
export TEST_CLEAN_ENV="${POP_TEST_CLEAN_ENV:=1}"

load "${TEST_COMMON_DIR}/integration-tests-common-funcs.sh"
load "${TEST_COMMON_DIR}/container-engine-funcs.sh"
load "${TEST_DIR}/parsec-openssl-provider-funcs.sh"

# Ensure that the state of the container environment is ready for the test
# suite
clean_test_environment() {

    # Use the BATS_TEST_NAME env var to categorize all logging messages relating
    # to the clean-up activities.
    export BATS_TEST_NAME="clean_test_environment"

    _run base_cleanup "${POP_TEST_IMAGE}"
    if [ "${status}" -ne 0 ]; then
        log "FAIL"
        return 1
    fi
}

@test 'pull image' {

    # Pull the test image locally
    # Might need to retry at least once with slow connections

    subtest="Pull the test image"
    for i in {1..3}; do
        _run image_pull "${POP_TEST_IMAGE}"
        if [ "${status}" -eq 0 ]; then
            break
        else
            echo "Timed out (${i}/3)"
        fi
    done
    if [ "${status}" -ne 0 ]; then
        log "FAIL" "${subtest}"
        return 1
    fi

    log "PASS" "${subtest}"
}

@test 'run container' {

    engine_args=(-v /run/parsec/parsec.sock:/tmp/parsec.sock -itd --name "${POP_TEST_CONTAINER_NAME}")

    subtest="Run the test container"
    _run container_run "${engine_args[@]}" "${POP_TEST_IMAGE}"
    if [ "${status}" -ne 0 ]; then
        log "FAIL" "${subtest}"
        return 1
    else
        log "PASS" "${subtest}"
    fi

    subtest="Check that the container is running"
    _run check_container_is_running "${POP_TEST_CONTAINER_NAME}"
    if [ "${status}" -ne 0 ]; then
        log "FAIL" "${subtest}"
        return 1
    else
        log "PASS" "${subtest}"
    fi

    subtest="Execute the health check"
    _run container_execute_bash_script "${POP_TEST_CONTAINER_NAME}" "${TEST_DIR}/health_check.sh"
    if [ "${status}" -ne 0 ]; then
        log "FAIL" "${subtest}"
        return 1
    else
        log "PASS" "${subtest}"
    fi

    log "PASS"
}

@test 'stop container' {

    subtest="Remove the running container"
    _run container_remove "${POP_TEST_CONTAINER_NAME}"
    if [ "${status}" -ne 0 ]; then
        log "FAIL" "${subtest}"
        return 1
    else
        log "PASS" "${subtest}"
    fi

    subtest="Check the container is not running"
    _run check_container_is_not_running "${POP_TEST_CONTAINER_NAME}"
    if [ "${status}" -ne 0 ]; then
        log "FAIL" "${subtest}"
        return 1
    else
        log "PASS" "${subtest}"
    fi

    log "PASS"
}
