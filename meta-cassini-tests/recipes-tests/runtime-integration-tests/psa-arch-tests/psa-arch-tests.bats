#!/usr/bin/env bats
#
# SPDX-FileCopyrightText: Copyright (c) 2023, Linaro Limited.
#
# SPDX-FileCopyrightText: <text>Copyright 2022 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

# psa-arch-tests (aka psa api tests).

if [ -z "${PSA_ARCH_TESTS_TEST_LOG_DIR}" ]; then
    TEST_LOG_DIR="${HOME}/runtime-integration-tests-logs"
else
    TEST_LOG_DIR="${PSA_ARCH_TESTS_TEST_LOG_DIR}"
fi

export TEST_LOG_FILE="${TEST_LOG_DIR}/psa-arch-tests.log"
export TEST_STDERR_FILE="${TEST_LOG_DIR}/psa-arch-tests-stderr.log"
export TEST_RUN_FILE="${TEST_RUNTIME_DIR}/psa-arch-tests.pgid"

export TEST_CLEAN_ENV="${PSA_ARCH_TESTS_TEST_CLEAN_ENV:=1}"

load "${TEST_COMMON_DIR}/integration-tests-common-funcs.sh"

@test 'PSA crypto api tests' {

    _run /usr/bin/psa-crypto-api-test
    if [ "${status}" -ne 0 ]; then
        log "FAIL"
        return 1
    else
        log "PASS"
    fi
}
@test 'PSA initial attestation api tests' {

    _run /usr/bin/psa-iat-api-test
    if [ "${status}" -ne 0 ]; then
        log "FAIL"
        return 1
    else
        log "PASS"
    fi
}
@test 'PSA internal trusted storage api tests' {

    _run /usr/bin/psa-its-api-test
    if [ "${status}" -ne 0 ]; then
        log "FAIL"
        return 1
    else
        log "PASS"
    fi
}
@test 'PSA protected storage api tests' {

    _run /usr/bin/psa-ps-api-test
    if [ "${status}" -ne 0 ]; then
        log "FAIL"
        return 1
    else
        log "PASS"
    fi
}
